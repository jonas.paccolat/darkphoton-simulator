==================
dps.generation.nlo
==================

Implementation of the NLO prediction of the differential and integrated cross
section.

Functions
---------

.. automodule:: dps.generation.nlo
	:members:
	:private-members:

# -*- coding: utf-8 -*-

import numpy as np

from dps import darkphoton
from dps.generation import npm, nlo
from dps.logs import log

def generate_masses(args):
    """
    Generate a range of masses according to the command arguments.
    """

    pdf = nlo.get_pdf()

    if args.mesh == "linear":
        dps = [darkphoton.DarkPhoton(M) for M in np.linspace(args.Mmin, args.Mmax, args.N)]
    elif args.mesh == "log":
        dps = [darkphoton.DarkPhoton(10**x)
               for x in np.linspace(np.log10(args.Mmin), np.log10(args.Mmax), args.N)]
    else:
        log.error("Invalid mesh={}. Try 'linear' or 'log'.".format(args.mesh))

    return dps

def central(args, scale=1., mem=0):
    """
    Compute the NPM and NLO central integrated cross section.

    Parameters
    ----------
    args: parser
        Namespace with the command line arguments.
    scale: float, optional
        The *NPM* and *NLO* differential cross section is computed at the scale
        *scale args.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0.

    Returns
    -------
    res: dict
        "M": array of dark photon masses.
        "npm": array of the NPM integrated cross section prediction.
        "nlo": array of the NLO integrated cross section prediction.
    """

    dps = generate_masses(args)
    n = len(dps)

    res = dict()
    res["M"] = np.array([dp.M for dp in dps])
    res["npm"] = np.zeros(n)
    res["nlo"] = np.zeros(n)

    for i, dp in enumerate(dps):

        log.info("{} %".format(100 * i // n))

        res["npm"][i] = npm.integrated_cross_section(dp, scale=scale, mem=mem)
        res["nlo"][i] = nlo.integrated_cross_section(dp, scale=scale, mem=mem)

    return res, args

def uncertainty(args, cl=95, Nscale=9, Dscale=2.):
    """
    Compute the PDF and scale uncertainties of the NPM single differential cross
    section.

    Parameters
    ----------
    args: parser
        Namespace with the command line arguments.
    cl: int, optional
        Confidence level (in percent) for the LHAPDF uncertainty
        computation. Default is 95.
    Nscale: int, optional
        Number of gradation in the scale interval. Default is 9.
    Dscale: float, optional
        The scales are varied between *args.M / Dscale* and *Dscale args.M*.
        Default is 2.

    Returns
    -------
    res: dict
        "M": array of dark photon masses.
        "npm": array of the NPM integrated cross section prediction. ("central", "pdf" and "scale")
        "nlo": array of the NLO integrated cross section prediction. ("central", "pdf" and "scale")
    """

    dps = generate_masses(args)

    _pdf = npm.get_pdf()

    n = len(dps)

    res = dict()
    res["M"] = np.array([dp.M for dp in dps])
    res["npm"] = {"central": list(), "pdf": list(), "scale": list()}
    res["nlo"] = {"central": list(), "pdf": list(), "scale": list()}

    for i, dp in enumerate(dps):

        log.info("{} %".format(100 * i // n))

        delta_pdf = {"npm": list(), "nlo": list()}
        for mem in range(_pdf.pset.size):
            delta_pdf["npm"].append(npm.integrated_cross_section(dp, scale=1, mem=mem))
            delta_pdf["nlo"].append(nlo.integrated_cross_section(dp, scale=1, mem=mem))

        for method in ["npm", "nlo"]:
            hessian = _pdf.pset.uncertainty(delta_pdf[method], cl)
            res[method]["central"].append(hessian.central)
            res[method]["pdf"].append((hessian.central - hessian.errminus,
                                       hessian.central + hessian.errplus))

        delta_npm = [npm.integrated_cross_section(dp, scale=Dscale**j) \
                              for j in np.linspace(-1, 1, Nscale)]
        delta_nlo = [nlo.integrated_cross_section(dp, scale=Dscale**j) \
                              for j in np.linspace(-1, 1, Nscale)]
        res["npm"]["scale"].append((min(delta_npm), max(delta_npm)))
        res["nlo"]["scale"].append((min(delta_nlo), max(delta_nlo)))

    return res, args

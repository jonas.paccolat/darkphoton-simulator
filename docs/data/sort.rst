=============
dps.data.sort
=============

Handle data.

Functions
---------

.. automodule:: dps.data.sort
	:members:
	:private-members:

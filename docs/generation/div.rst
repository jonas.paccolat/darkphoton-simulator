==================
dps.generation.div
==================

Implementation of the DIV prediction of the differential cross section.

Functions
---------

.. automodule:: dps.generation.div
	:members:
	:private-members:

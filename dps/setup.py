# -*- coding: utf-8 -*-

import numpy as np

class Setup:
    """
    Class containing the parameters of the proton collision device.
    """

    # beam momentum in GeV (SHiP)
    P_beam = 400.
    # number of active flavors (SHiP)
    nf = 3
    # proton mass [GeV]
    m_proton = 0.938
    # fine structure constant
    aEM = 1. / 137.
    # Conversion ratio from GeV^-2 to mb
    UnitConversion = 1. / 2.56816
    # electric charge fractions: gluon, down, up, strange, charm, bottom, top
    e = np.array([0., 1. / 3., 2. / 3., 1. / 3., 2. / 3., 1. / 3., 2. / 3.])
    # group properties
    NC = 3
    CF = 4. / 3.

    def __init__(self):
        self.E_beam, self.S_beam = self.init_energy()
        self.sigma0 = self.init_born_interaction()
        self.E_CM, self.P_CM = self.init_cm_energy()
        self.y0 = self.init_relative_rapidity()

    def init_energy(self):
        """
        Compute the beam energy and the collision center of mass energy.

        Returns
        -------
        E_beam: float
            Beam energy.
        S_beam:
            Collision center of mass energy.
        """

        E_beam = np.sqrt(self.m_proton**2 + self.P_beam**2)
        S_beam = 2 * self.m_proton**2 + 2 * self.m_proton * E_beam

        return E_beam, S_beam

    def init_born_interaction(self):
        """
        Compute the Drell-Yan leading order (Born) coefficient.

        Returns
        -------
        sigma0: float
            Born interaction term.
        """

        sigma0 = 4 / 3 * np.pi**2 * self.aEM * self.e**2

        return self.UnitConversion * sigma0

    def init_cm_energy(self):
        """
        Compute the protons energy and momentum in the center of mass frame.

        Returns
        -------
        E_CM: float
            Proton energy in the CM frame.
        P_CM: float
            Proton momentum in the CM frame.
        """

        E_CM = np.sqrt(self.S_beam) / 2
        P_CM = np.sqrt(self.m_proton / 2 * (self.E_beam - self.m_proton))

        return E_CM, P_CM

    def init_relative_rapidity(self):
        """
        Compute the rapidity separating the center of mass frame from the lab
        frame:

        .. math::
            y_0 = y_{CM}-y_{lab}

        Returns
        -------
        y0: float
            Rapidity separating the center of mass frame from the lab frame.
        """

        y0 = np.log((self.E_CM - self.P_CM) / self.m_proton)

        return y0

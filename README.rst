=====================
Dark Photon Simulator
=====================

Simulation of dark photons generated in hard proton collision at fixed target
experiments.

Link to `full documentation
<https://darkphoton-simulator.readthedocs.io/en/latest/>`_.

==============
dps.generation
==============

Subpackage for computing the differential and integrated cross section
predictions.

.. toctree::
   :maxdepth: 1

   generation/css
   generation/ddcs
   generation/div
   generation/ics
   generation/nlo
   generation/npm
   generation/pdf
   generation/sdcs

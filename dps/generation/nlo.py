# -*- coding: utf-8 -*-

import numpy as np
from scipy.integrate import quad

from dps import setup
from dps.generation import kernel
from dps.generation import pdf
from dps.logs import log

_pdf = pdf.PDF("CT14nlo")
_setup = setup.Setup()

def get_pdf():
    return _pdf

### differential cross section

def t_limit(dp):
    """
    Boundaries of the Mandelstam variable t (at the partonic level) which is
    used as the integration variable.
    """

    tmin = dp.M**2 - dp.M * dp.MT / dp.xB
    tmax = dp.M / (dp.xA * dp.MT - dp.M) * dp.kT**2

    return tmin, tmax

def amplitude(dp, x, y, z):
    """
    Recurent expression in the NLO amplitude squared terms.
    """

    return 2 * x * dp.M**2 / (y * z) + y / z + z / y

def xiA(dp, t):
    """Parton A longitudinal fraction as function of t."""

    return dp.xA * dp.MT / dp.M * t / (dp.kT**2 + t)

def xiB(dp, t):
    """Parton B longitudinal fraction as function of t."""

    return dp.xB / dp.M / dp.MT * (dp.M**2 - t)

def ustar(dp, t):
    """Mandelstam u as function of t."""

    return (dp.M**2 - t) / (dp.kT**2 + t) * dp.kT**2

def sstar(dp, t):
    """Mandelstam s as function of t."""

    return ustar(dp, t) * t / dp.kT**2

def t_integrand(t, dp, Q2, mem):
    """Integrand of the integral over t"""

    xiA_ = xiA(dp, t)
    xiB_ = xiB(dp, t)
    ustar_ = ustar(dp, t)
    sstar_ = sstar(dp, t)

    tot = 0.
    for iParton in range(1, _setup.nf + 1):

        fqq = _pdf.pdf(xiA_, iParton, Q2, mem) \
            * _pdf.pdf(xiB_, -iParton, Q2, mem) \
            + _pdf.pdf(xiA_, -iParton, Q2, mem) \
            * _pdf.pdf(xiB_, iParton, Q2, mem)
        fqg = _pdf.pdf(xiA_, iParton, Q2, mem) \
            * _pdf.pdf(xiB_, 0, Q2, mem) \
            + _pdf.pdf(xiA_, -iParton, Q2, mem) \
            * _pdf.pdf(xiB_, 0, Q2, mem)
        fgq = _pdf.pdf(xiA_, 0, Q2, mem) \
            * _pdf.pdf(xiB_, iParton, Q2, mem) \
            + _pdf.pdf(xiA_, 0, Q2, mem) \
            * _pdf.pdf(xiB_, -iParton, Q2, mem)

        tot += _setup.sigma0[iParton] * _pdf.alpha.alphasQ2(Q2) \
             * ( \
                32. / 3 * fqq * amplitude(dp, sstar_, t, ustar_) \
              - 4. * fgq * amplitude(dp, ustar_, sstar_, t) \
              - 4. * fqg * amplitude(dp, t, ustar_, sstar_) \
               )

    prefactor = 1 / t / (dp.M**2 - t)

    return prefactor*tot

def differential_cross_section(dp, scale=1, mem=0, eps=1e-3):
    """
    Compute the NLO double differential cross section prediction.

    Parameters
    ----------
    dp: dps.darkphoton.DarkPhoton
        DarkPhoton instance for which the differential cross section is computed.
    scale: float, optional
        The PDF is evaluated at the energy scale: *scale dp.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0 (central value).
    eps: float, optional
        Numerical relative error. Default is 1e-3.

    Returns
    -------
    float: differential cross section
    """

    if dp.kT == 0:
        return np.nan

    Q2 = scale * dp.M**2
    if Q2 < _pdf.q2Min:
        log.warning("Interaction scale below the PDF lower bound for M={}.".format(dp.M))

    prefactor = _setup.P_beam / dp.Ek / (16 * np.pi * _setup.S_beam)
    tmin, tmax = t_limit(dp)
    args = (dp, Q2, mem)
    dcs = quad(t_integrand, tmax, tmin, args=args, \
               epsrel=eps, epsabs=0, limit=1000)[0]

    return prefactor * dcs

### integrated cross section

def first_integrand(z, xiA, dp, Q2, mem=0):
    """First integral of the integrated cross section."""

    tau = dp.M**2 / _setup.S_beam
    zmin = tau / xiA

    totB = 0.
    if z >= 1:
        return 0

    elif z <= zmin:
        for iParton in range(1, _setup.nf+1):

            totB -= _setup.sigma0[iParton] / _setup.S_beam \
                  * ( \
                     _pdf.pdf(xiA, iParton, Q2, mem) \
                   * _pdf.pdf(zmin, -iParton, Q2, mem) \
                   + _pdf.pdf(xiA, -iParton, Q2, mem) \
                   * _pdf.pdf(zmin, iParton, Q2, mem) \
                    ) \
                 * ( \
                    np.log(dp.M**2 / Q2) * kernel.Pqq_plus(1) / (1-z) \
                  + kernel.Dq_plus(1) * np.log(1-z) / (1-z) \
                   )

        return _pdf.alpha.alphasQ2(Q2) / np.pi / _setup.S_beam * totB

    elif z > zmin:
        for iParton in range(1, _setup.nf+1):

            totB += _setup.sigma0[iParton] / z \
                  * ( \
                     _pdf.pdf(xiA, iParton, Q2, mem) \
                   * _pdf.pdf(zmin/z, -iParton, Q2, mem) \
                   + _pdf.pdf(xiA, -iParton, Q2, mem) \
                   * _pdf.pdf(zmin/z, iParton, Q2, mem) \
                    ) \
                  * ( \
                     np.log(dp.M**2/Q2) * kernel.Pqq_plus(z) / (1-z) \
                   + kernel.Dq_plus(z) * np.log(1-z) / (1-z) \
                   + kernel.Dq_fin(z) \
                    )

            totB += _setup.sigma0[iParton] / z \
                  * ( \
                     _pdf.pdf(xiA, -iParton, Q2, mem) \
                   * _pdf.pdf(zmin/z, 0, Q2, mem) \
                   + _pdf.pdf(xiA, 0, Q2, mem) \
                   * _pdf.pdf(zmin/z, -iParton, Q2, mem) \
                   + _pdf.pdf(xiA, iParton, Q2, mem) \
                   * _pdf.pdf(zmin/z, 0, Q2, mem) \
                   + _pdf.pdf(xiA, 0, Q2, mem) \
                   * _pdf.pdf(zmin/z, iParton, Q2, mem) \
                    ) \
                   * (np.log(dp.M**2/Q2) * kernel.Pqg(z) + kernel.Dg(z))

            totB -= _setup.sigma0[iParton] \
                  * ( \
                     _pdf.pdf(xiA, iParton, Q2, mem) \
                   * _pdf.pdf(zmin, -iParton, Q2, mem) \
                   + _pdf.pdf(xiA, -iParton, Q2, mem) \
                   * _pdf.pdf(zmin, iParton, Q2, mem) \
                    ) \
                  * ( \
                     np.log(dp.M**2/Q2) * kernel.Pqq_plus(1) / (1-z) \
                   + kernel.Dq_plus(1) * np.log(1-z) / (1-z) \
                    )

        return _pdf.alpha.alphasQ2(Q2) / np.pi / _setup.S_beam * totB

def second_integrand(xiA, dp, Q2, mem=0, eps=1e-2):
    """Second integral of the integrated cross section."""

    tau = dp.M**2 / _setup.S_beam

    tot = quad(first_integrand, 0., 1., \
               args=(xiA, dp, Q2, mem), \
               epsrel=eps, epsabs=0, limit=1000)[0]

    for iParton in range(1, _setup.nf + 1):

        tot += _setup.sigma0[iParton] / _setup.S_beam \
             * ( \
                _pdf.pdf(xiA, iParton, Q2, mem) \
              * _pdf.pdf(tau/xiA, -iParton, Q2, mem) \
              + _pdf.pdf(xiA, -iParton, Q2, mem) \
              * _pdf.pdf(tau/xiA, iParton, Q2, mem) \
                ) \
             * ( \
                1 + _pdf.alpha.alphasQ2(Q2) / np.pi \
              * (np.log(dp.M**2/Q2) * kernel.Pqq_div + kernel.Dq_div) \
               )

    return tot / xiA

def integrated_cross_section(dp, scale=1, mem=0, eps=1e-2):
    """
    Compute the NLO integrated cross section prediction.

    Parameters
    ----------
    dp: dps.darkphoton.DarkPhoton
        DarkPhoton instance for which the integrated cross section is computed.
    scale: float, optional
        The PDF is evaluated at the energy scale: *scale dp.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0 (central value).
    eps: float, optional
        Numerical relative error. Default is 1e-2.

    Returns
    -------
    float: integrated cross section
    """

    Q2 = scale * dp.M**2
    if Q2 < _pdf.q2Min:
        log.warning("Interaction scale below the PDF lower bound for M={}.".format(dp.M))

    tau = dp.M**2 / _setup.S_beam

    ics = quad(second_integrand, tau, 1, \
               args=(dp, Q2, mem, eps), \
               epsrel=eps, epsabs=0, limit=1000)[0]

    return ics

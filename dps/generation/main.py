# -*- coding: utf-8 -*-

import os
import pickle
import argparse

def execute(args):

    if args.d == 0:
        assert args.Mmin is not None, "Please, set the mass argument: --Mmin"
        assert args.Mmax is not None, "Please, set the mass argument: --Mmax"
        from dps.generation import ics
        if args.u == 0:
            res, args = ics.central(args)
        else:
            res, args = ics.uncertainty(args)
    elif args.d == 1:
        assert args.M is not None, "Please, set the mass argument: --M"
        from dps.generation import sdcs
        if args.u == 0:
            res, args = sdcs.central(args)
        else:
            res, args = sdcs.uncertainty(args)
    else:
        assert args.M is not None, "Please, set the mass argument: --M"
        from dps.generation import ddcs
        if args.u == 0:
            res, args = ddcs.central(args)
        else:
            res, args = ddcs.uncertainty(args)

    return res, args

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--d", type=int, required=True, help="Cross section type. 0: integrated, 1: single differential., 2: double differential.")
    parser.add_argument("--u", type=int, default=0, help="Uncertainty. 0: No, 1: Yes.")

    parser.add_argument("--M", type=float, default=None, help="Dark photon mass [GeV].")
    parser.add_argument("--Mmin", type=float, default=None, help="Smallest dark photon mass [GeV].")
    parser.add_argument("--Mmax", type=float, default=None, help="Largest dark photon mass [GeV].")

    parser.add_argument("--mesh", type=str, help="Coordinate gradation type. \"constant\" (u=1,2), \"gaussian\" (u=1,2), \"random\" (u=2), \"linear\" (u=0) or \"log\" (u=0)")
    parser.add_argument("--N", type=int, help="Sample size")
    parser.add_argument("--Nz", type=int, help="Sample size along z.")
    parser.add_argument("--NkT", type=int, help="Sample size along kT.")

    parser.add_argument("--seed", type=int, default=0, help="Random seed for random sample (u=2).")

    parser.add_argument("--pickle", type=str, required=True, help="Path to output.")


    args = parser.parse_args()

    assert (args.u == 0 or args.u == 1), "Invalid argument: u={}.".format(args.u)
    assert (args.d == 0 or args.d == 1 or args.d == 2), "Invalid argument: d={}.".format(args.d)

    try:
        res, args = execute(args)

        with open(args.pickle, 'wb') as f:
            pickle.dump(args, f)
            pickle.dump(res, f)
    except:
        os.remove(args.pickle)
        raise


if __name__ == "__main__":
    main()

# -*- coding: utf-8 -*-

import numpy as np

from dps import sample, darkphoton
from dps.generation import npm
from dps.logs import log

def generate_momenta(args):
    """
    Generate momenta covering the physical domain of the given mass with the
    given mesh.
    """

    dp = darkphoton.DarkPhoton(args.M)

    if args.mesh == "constant":
        sample.sample(dp, "cut", args.N, kT=0., mesh="constant",
                      set_coord=True, quiet=True)
    elif args.mesh == "gaussian":
        sample.sample(dp, "cut", args.N, kT=0., mesh="gaussian",
                      set_coord=True, quiet=True)
    else:
        log.error("Invalid mesh={}. Try 'constant' or 'gaussian'.".format(args.mesh))


    return dp

def central(args, scale=1., mem=0):
    """
    Compute the NPM central single differential cross section.

    Parameters
    ----------
    args: parser
        Namespace with the command line arguments. It fixes the dark photon mass
        and the covering of the physical domain.
    scale: float, optional
        The *NPM* differential cross section is computed at the scale
        *scale args.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0.

    Returns
    -------
    res: dict
        "z": array of the longitudinal fractions.
        "kT": array of the transverse momenta.
        "npm": array of the NPM differential cross section prediction.
    """

    dp = generate_momenta(args)

    n = len(dp.z)

    res = dict()
    res["z"] = dp.z
    res["npm"] = np.zeros(n)

    for i in range(n):

        log.info("{} %".format(100 * i // n))

        _dp = dp.select(i)
        res["npm"][i] = npm.differential_cross_section(_dp, scale=scale, mem=mem)

    return res, args

def uncertainty(args, cl=95, Nscale=9, Dscale=2.):
    """
    Compute the PDF and scale uncertainties of the NPM single differential cross
    section.

    Parameters
    ----------
    args: parser
        Namespace with the command line arguments. It fixes the dark photon mass
        and the covering of the physical domain.
    cl: int, optional
        Confidence level (in percent) for the LHAPDF uncertainty
        computation. Default is 95.
    Nscale: int, optional
        Number of gradation in the scale interval. Default is 9.
    Dscale: float, optional
        The scales are varied between *args.M / Dscale* and *Dscale args.M*.
        Default is 2.

    Returns
    -------
    res: dict
        "z": array of the longitudinal fractions.
        "kT": array of the transverse momenta.
        "css": array of the CSS differential cross section prediction. ("central" and "pdf")
        "div": array of the DIV differential cross section prediction. ("central", "pdf" and "scale")
        "nlo": array of the NLO differential cross section prediction. ("central", "pdf" and "scale")
    """

    dp = generate_momenta(args)

    _pdf = npm.get_pdf()

    n = len(dp.z)

    res = dict()
    res["z"] = dp.z
    res["npm"] = {"central": list(), "pdf": list(), "scale": list()}

    for i in range(n):

        log.info("{} %".format(100 * i // n))

        _dp = dp.select(i)

        # PDF uncertainty
        delta_pdf = list()
        for mem in range(_pdf.pset.size):
            delta_pdf.append(npm.differential_cross_section(_dp, scale=1, mem=mem))

        hessian = _pdf.pset.uncertainty(delta_pdf, cl)
        res["npm"]["central"].append(hessian.central)
        res["npm"]["pdf"].append((hessian.central - hessian.errminus,
                                  hessian.central + hessian.errplus))

        # scale uncertainty

        delta_scale = [npm.differential_cross_section(_dp, scale=Dscale**j) \
                                        for j in np.linspace(-1, 1, Nscale)]
        res["npm"]["scale"].append((float(min(delta_scale)),
                                    float(max(delta_scale))))

    return res, args

===========
dps.fit.dcs
===========

In this module the fit class *DCS* is defined. Once instantiated for a given
mass it allows a fast computation of the differential cross section. It is most
efficient for arrays of more than :math:`10^6` momenta where it takes
:math:`10^{-5} \: s` per momentum.

The actual physical domain is slightly reduced because of fitting instabilities
in regions of suppressed differential cross section (gray domain on the left
figure).

Two procedures are implemented for the matching between the small and large
:math:`k_T` regimes giving a superior and inferior bound on the differential
cross section. The superior / inferior bound (*sup* / *inf*) is obtained by
considering the *css* / *nlo* prediction in the transition region (blue domain
on the left figure). The transition lines *kT1* and *kT2* are defined as the
transverse momenta where the *css* domination over the *nlo* starts (first
intersection) and respectively where it ends (second intersection), as pictured
on the right figure.

.. figure:: ../figures/domain.png


Class
-----

.. autoclass:: dps.fit.dcs.DCS
	:members:
	:private-members:

===================
dps.generation.ddcs
===================

Module to cover the double differential cross section domain and compute its
different predictions.

Functions
---------

.. automodule:: dps.generation.ddcs
	:members:
	:private-members:

================
dps.distribution
================

Module to regularly sample coordinates according to the shifted Gaussian
distribution

.. math::

      f = A(e^{-(x - x_0)^2 / 2 \sigma^2} + a)

The shift constant :math:`a` and the normalization :math:`A` are adjusted to get
the chosen last step interval and the chosen number of steps.

Functions
---------

.. automodule:: dps.distribution
	:members:
	:private-members:

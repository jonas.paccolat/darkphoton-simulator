# -*- coding: utf-8 -*-

from scipy.integrate import quad

from dps import darkphoton, setup
from dps.generation import pdf
from dps.logs import log

_pdf = pdf.PDF("CT14nlo")
_setup = setup.Setup()

def get_pdf():

    return _pdf

### differential cross section

def differential_cross_section(dp, scale=1, mem=0):
    """
    Compute the NPM single differential cross section prediction.

    Parameters
    ----------
    dp: dps.darkphoton.DarkPhoton
        DarkPhoton instance for which the differential cross section is computed.
    scale: float, optional
        The PDF is evaluated at the energy scale: *scale dp.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0 (central value).
    """

    assert dp.kT == 0, 'At LO the dark photon has no transverse momentum.'

    Q2 = scale * dp.M**2
    if Q2 < _pdf.q2Min:
        log.warning("Interaction scale below the PDF lower bound for M={}.".format(dp.M))

    tot = 0.
    for iParton in range(1, _setup.nf + 1):

        tot += _setup.sigma0[iParton] \
             * (
                _pdf.pdf(dp.xA, iParton, Q2, mem) \
              * _pdf.pdf(dp.xB, -iParton, Q2, mem) \
              + _pdf.pdf(dp.xA, -iParton, Q2, mem) \
              * _pdf.pdf(dp.xB, iParton, Q2, mem)
               )

    prefactor = _setup.P_beam / dp.Ek / _setup.S_beam

    return prefactor * tot

### integrated cross section

def integrand(z, M, scale, mem):
    """
    Differential cross section as function of z to be integrated.
    """

    dp = darkphoton.DarkPhoton(M, z, 0)
    dp.set_kinematics()
    dcs = differential_cross_section(dp, scale, mem)

    return dcs

def integrated_cross_section(dp, scale=1, mem=0, eps=1e-3):
    """
    Compute the NPM integrated cross section prediction.

    Parameters
    ----------
    dp: dps.darkphoton.DarkPhoton
        DarkPhoton instance for which the differential cross section is computed.
    scale: float, optional
        The PDF is evaluated at the energy scale: *scale dp.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0 (central value).
    eps: float, optional
        Numerical relative error. Default is 1e3.
    """

    zmin, zmax = dp.get_longitudinal_fraction_limit()
    args = (dp.M, scale, mem)
    ics = quad(integrand, zmin, zmax, args=args,\
                        epsrel=eps, epsabs=0, limit=1000)[0]

    return ics

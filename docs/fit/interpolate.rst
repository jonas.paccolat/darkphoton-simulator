===================
dps.fit.interpolate
===================

In this module a list of functions for the mass interpolation of the
differential cross section are defined. The differential cross section fit at
fixed mass are also defined in this module.

Functions
---------

.. automodule:: dps.fit.interpolate
	:members:
	:private-members:

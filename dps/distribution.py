# -*- coding: utf-8 -*-

import numpy as np
from scipy.special import erf
from scipy.optimize import fsolve

from dps.logs import log

def shifted_gaussian_distribution(x0, sigma, a, A):
    """
    Define the shifted gaussian distribution function.

    Parameters
    ----------
    x0: float
        Mean of the gaussian.
    sigma: float
        Standard deviation of the gaussian.
    a: float
        Shift constant.
    A: float
        Normalisation.

    Returns
    -------
    f: func
        Shifted gaussian distribution function.
    """

    func = lambda x: A*(np.exp(-(x-x0)**2/(2*sigma**2))+a)

    return func

def get_shift_constant_from_step(xmin, xmax, x0, sigma, N, step):
    """
    Find the shift constant required for the last step to be equal to *step*.

    Parameters
    ----------
    xmin: float
        Lower limit of the definition interval.
    xmax: float
        Upper limit of the definition interval.
    x0: float
        Mean of the gaussian. Must lie between *xmin* and *xmax*.
    sigma: float
        Standard deviation of the gaussian.
    N: int
        Number of steps to cover the interval [*xmin*,*xmax*].
    step: float
        Size of the last step.

    Returns
    -------
    a: float
        Shift constant of the distribution.
    """

    x1 = erf((xmax-x0)/(np.sqrt(2)*sigma))
    x2 = erf((x0-xmin)/(np.sqrt(2)*sigma))

    y1 = erf((xmax-x0)/(np.sqrt(2)*sigma))
    y2 = erf((xmax-x0-step)/(np.sqrt(2)*sigma))

    X = sigma*np.sqrt(np.pi/2)*(x1+x2-N*(y1-y2))
    Y = N*step-(xmax-xmin)

    return X/Y

def get_normalization(xmin, xmax, x0, sigma, N, a):
    """
    Find the normalisation constant required to split the interval in *N* steps
    for a given shift constant *a*.

    Parameters
    ----------
    xmin: float
        Lower limit of the definition interval.
    xmax: float
        Upper limit of the definition interval.
    x0: float
        Mean of the gaussian. Must lie between *xmin* and *xmax*.
    sigma: float
        Standard deviation of the gaussian.
    N: int
        Number of steps to cover the interval [*xmin*,*xmax*].
    a: float
        Shift constant.

    Returns
    -------
    N: float
        Normalisation of the distribution.
    """

    dz = xmax-xmin
    x1 = erf((xmax-x0)/(np.sqrt(2)*sigma))
    x2 = erf((x0-xmin)/(np.sqrt(2)*sigma))

    X = N
    Y = sigma*np.sqrt(np.pi/2)*(x1+x2)+a*dz

    return X/Y

def real_value_index(x, xmin, x0, sigma, a, A):
    """
    Compute the integral which matches the interval coordinate *x* to the index
    associated to the step it belongs to.

    When the integral output is the integer *i* its upper boundary *x* is the
    coordinate of the lower limit of the *i* st step.

    Parameters
    ----------
    x: float
        Upper boundary of the integral.
    xmin: float
        Lower limit of the definition interval.
    x0: float
        Mean of the gaussian. Must lie between *xmin* and *xmax*.
    sigma: float
        Standard deviation of the gaussian.
    a: float
        Shift constant.
    A: float
        Normalisation.

    Returns
    -------
    i: float
        Real valued step index.
    """

    x1 = erf((x-x0)/(np.sqrt(2)*sigma))
    x2 = erf((x0-xmin)/(np.sqrt(2)*sigma))
    X = sigma*np.sqrt(np.pi/2)*(x1+x2)+a*(x-xmin)

    return A*X

def get_coordinates(xmin, x0, sigma, N, a, A):
    """
    Iteratively find the step coordinates along the input interval.

    Parameters
    ----------
    xmin: float
        Lower limit of the definition interval.
    x0: float
        Mean of the gaussian. Must lie between *xmin* and *xmax*.
    sigma: float
        Standard deviation of the gaussian.
    N: int
        Number of steps to cover the interval [*xmin*,*xmax*].
    a: float
        Shift constant.
    A: float
        Normalisation.

    Returns
    -------
    xrange: list
        Coordinates of the steps covering the input interval.
    """

    xrange = [xmin]
    for i in range(1, N+1):

        def func(x):
            x = real_value_index(x, xmin, x0, sigma, a, A)
            return x-i

        x = fsolve(func, x0)[0]
        xrange.append(x)

    return xrange

def get_astar(xmin, xmax, x0, sigma):
    """
    Return the minimal value of the shift constant so that the associated
    normalisation is positive.

    Parameters
    ----------
    xmin: float
        Lower limit of the definition interval.
    xmax: float
        Upper limit of the definition interval.
    x0: float
        Mean of the gaussian. Must lie between *xmin* and *xmax*.
    sigma: float
        Standard deviation of the gaussian.

    Returns
    -------
    astar: float
        Minimal shift constant leading to a positive noramlisation.
    """

    x1 = erf((xmax-x0)/(np.sqrt(2)*sigma))
    x2 = erf((x0-xmin)/(np.sqrt(2)*sigma))

    amin = -sigma*np.sqrt(np.pi/2)*(x1+x2)/(xmax-xmin)

    return amin

def get_amin(xmin, xmax, x0, sigma):
    """
    Return the minimal shift constant which ensures that the distribution is
    positive on both input interval boundaries.

    Parameters
    ----------
    xmin: float
        Lower limit of the definition interval.
    xmax: float
        Upper limit of the definition interval.
    x0: float
        Mean of the gaussian. Must lie between *xmin* and *xmax*.
    sigma: float
        Standard deviation of the gaussian.

    Returns
    -------
    amin: float
        Minimal shift constant leading to a positive distribution on the
        boundaries.
    """

    dx_star = max(x0-xmin, xmax-x0)

    return - np.exp(-dx_star**2/(2*sigma**2))

def get_step_limits(xmin, xmax, x0, sigma, N):
    """
    Compute the minimal and maximal values of the step size leading to a
    coherent distribution (positive and concave).

    Parameters
    ----------
    xmin: float
        Lower limit of the definition interval.
    xmax: float
        Upper limit of the definition interval.
    x0: float
        Mean of the gaussian. Must lie between *xmin* and *xmax*.
    sigma: float
        Standard deviation of the gaussian.
    N: int
        Number of steps to cover the interval [*xmin*,*xmax*].

    Returns
    -------
    step_min: float
        Minimal step size.
    step_max: float
        Maximal step size.
    """

    step_min = (xmax - xmin) / N
    amin = get_amin(xmin, xmax, x0, sigma)
    f = lambda step: get_shift_constant_from_step(xmin, xmax, x0, sigma, N, step) - amin
    step_max = float(min(fsolve(f, 2*step_min), xmax - xmin))

    return step_min, step_max

def gradation(xmin, xmax, x0, sigma, N, step):
    """
    Find the *N+1* gradation coordinates of the interval [*xmin*,*xmax*]
    following the shifted gaussian distribution of parameters *x0* and *sigma*,

    .. math::

        f = A(e^{-(x - x_0)^2 / 2 \\sigma^2} + a)


    The shift constant *a* is chosen so that the last interval is of length
    *step* and the noramlisation *A* is adjsuted for the number of intervals to
    be equal to *N*.

    Parameters
    ----------
    xmin: float
        Lower interval boundary coordinate.
    xmax: float
        Upper interval boundary coordinate.
    x0: float
        Mean of the gaussian distribution.
    sigma: float
        Variance of the gaussian distribution.
    N: int
        Number of subintervals to split the original interval into.
    step: float
        Length of the last subinterval.

    Returns
    -------
    xrange: list
        List of the gradation coordinates.
    """

    # check consistency of the input parameters
    step_min, step_max = get_step_limits(xmin, xmax, x0, sigma, N)
    if not step_min < step < step_max:
        text = 'The provided step size {} would lead to a non consistent '\
               'distribution. Please choose a value in the range [{},{}].'
        raise DistributionError(text.format(step, step_min, step_max))

    # get distribution parameters
    a = get_shift_constant_from_step(xmin, xmax, x0, sigma, N, step)
    A = get_normalization(xmin, xmax, x0, sigma, N, a)

    # get gradation
    xrange = get_coordinates(xmin, x0, sigma, N, a, A)

    return xrange

class DistributionError(Exception):
    pass

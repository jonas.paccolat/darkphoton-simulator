=======
dps.fit
=======

Subpackage for fitting the differential and integrated cross section.

.. toctree::
   :maxdepth: 1

   fit/dcs
   fit/ics
   fit/interpolate

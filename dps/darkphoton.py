# -*- coding: utf-8 -*-

import os
import copy
import numpy as np
from scipy.interpolate import interp1d

from dps.logs import log
from dps import utils, setup
_setup = setup.Setup()

class DarkPhoton:
    """
    Class defining the properties of a dark photon of fixed mass.

    The physical region in the (z, kT)-plane is computed and is used to decide
    the physicality of any momentum.

    Parameters
    ----------
    M: float
        Mass of the dark photon (in GeV).
    z: None, float or array, optional
        Longitudinal fraction(s). Default is None.
    kT: None, float or array, optional
        Transverse momentum(a) (in GeV). Default is None.
    """

    def __init__(self, M, z=None, kT=None):
        self.M = M
        self.z = z
        self.kT = kT
        self.zmin, self.zmax = self.get_longitudinal_fraction_limit()
        self.abskTmax = self.get_abskTmax()
        self.z0 = self.get_z0()
        self.kTmax = self.get_kTmax()
        self.yCM = None
        self.xA = None
        self.xB = None
        self.MT = None
        self.kL = None
        self.Ek = None
        self.indomain = None

    def set_kinematics(self):
        """
        Set the values of the relevant kinematic variables according to the
        DarkPhoton *z* and *kT*.
        """

        if (self.z is not None) and (self.kT is not None):
            yCM, xA, xB, MT, kL, Ek = utils.get_kinematics(self.M, self.z, self.kT)
            self.yCM = yCM
            self.xA = xA
            self.xB = xB
            self.MT = MT
            self.kL = kL
            self.Ek = Ek
        else:
            raise DarkPhotonError("Momentum is required.")

    def clear_kinematics(self):
        """
        Reset all kinematic variables to *None*.
        """

        self.yCM = None
        self.xA = None
        self.xB = None
        self.MT = None
        self.kL = None
        self.Ek = None

    def get_longitudinal_fraction_limit(self):
        """
        Compute the absolute minimal and maximal value of the longitudinal
        fraction for the considered dark photon mass.

        Returns
        -------
        list
            (zmin, zmax)
        """

        zmin = self.M**2 * np.exp(-_setup.y0)\
               / (2*_setup.P_beam * np.sqrt(_setup.S_beam))\
               - np.sqrt(_setup.S_beam) * np.exp(_setup.y0)\
               / (2*_setup.P_beam)

        zmax = -self.M**2 * np.exp(_setup.y0)\
               / (2*_setup.P_beam * np.sqrt(_setup.S_beam))\
               + np.sqrt(_setup.S_beam) * np.exp(-_setup.y0)\
               / (2*_setup.P_beam)

        return zmin, zmax

    def get_abskTmax(self):
        """
        Compute the absolute maximal value of the transverse momentum for the
        considered dark photon mass.

        Returns
        -------
        float:
            Absolute maximal kT value.
        """

        abskTmax = (_setup.S_beam - self.M**2) / (2*np.sqrt(_setup.S_beam))

        return abskTmax

    def get_z0(self):
        """
        Get the approximate longitudinal fraction of the differential cross
        section maximum.

        This quantity is obtained from fitted data.
        """

        A = 1.7696
        B = 0.0047
        f = lambda x: B * x**A
        z0 = f(self.M)

        return z0

    def set_indomain(self, eps=0):
        """
        Set the attribute *indomain* to True if the DarkPhoton momenta lie
        inside the physical domain.

        The on-shell condition and the energy condition are checked.
        """

        if self.yCM is not None:
            cond = utils.get_indomain(self.M, self.kT, self.MT,
                                self.xA, self.xB, self.yCM, eps)
            self.indomain = cond
        else:
            text = "The kinematics must be set first. \
                    Use DarkPhoton.set_kinematics()."
            raise DarkPhotonError(text)

    def clear_indomain(self):
        """
        Reset *indomain* attribute to *None*.
        """

        self.indomain = None

    def get_kTmax(self):
        """
        Return the function defining the upper *kT* boundary. It is a function
        of *z*.

        It is set to the attribute *kTmax* at initialization.
        """

        # todo: find analytic expression

        n = 10000
        kT = np.linspace(0, self.abskTmax, n)
        kT, (zmin, zmax) = self.get_zmin_and_zmax(kT)

        z = np.concatenate((zmin[:-1], np.flip(zmax)))
        kT = np.concatenate((kT[:-1], np.flip(kT)))

        return interp1d(z, kT, kind=2, bounds_error=False, fill_value=0)

    def get_zmin_and_zmax(self, kT):
        """
        Computes the minimal and maximal *z* values for the given *kT*.

        Parameters
        ----------
        kT: float, array_like
            Transverse momentum(a) (in GeV).

        Returns
        -------
        kT: array
            Subset of the given *kT*. Only the physical ones are considered.
        (zmin, zmax): (array, array)
            Minimal and maximal longitudinal fractions associated to the
            returned *kT*.
        """

        MT = np.sqrt(self.M**2 + kT**2)
        y = np.arccosh(0.5 * np.sqrt(_setup.S_beam) / MT * (1 + self.M**2 / _setup.S_beam))
        b = np.exp(y) + self.M**2 / _setup.S_beam * np.exp(-y) > 0

        amax = np.exp(-2 * (_setup.y0 - y))
        amin = np.exp(-2 * (_setup.y0 + y))

        zmax = np.abs(0.5 * MT / _setup.P_beam * (1 - amax) / np.sqrt(amax))
        zmin = np.abs(0.5 * MT / _setup.P_beam * (1 - amin) / np.sqrt(amin))

        if type(kT) == float or type(kT) == int:
            assert b, "Invalid kT={}: out of the phyiscal domain.".format(kT)
            return kT, (zmin, zmax)
        else:
            return kT[b], (zmin[b], zmax[b])

    def select(self, i):

        dp = self.__copy__()
        dp.z = self.z[i]
        dp.kT = self.kT[i]
        dp.set_kinematics()

        return dp

    def __copy__(self):
        return DarkPhoton(self.M)

class DarkPhotonError(Exception):
    """Base class for DarkPhoton exceptions."""
    pass

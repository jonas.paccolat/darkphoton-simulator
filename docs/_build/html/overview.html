
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>Overview &#8212; darkphoton simulator 1.0.0 documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/language_data.js"></script>
    <script async="async" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="DarkPhotonSimulator" href="dps.html" />
    <link rel="prev" title="Welcome to DarkPhotonSimulator’s documentation!" href="index.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="overview">
<h1>Overview<a class="headerlink" href="#overview" title="Permalink to this headline">¶</a></h1>
<p>This package simulates the production of dark photons in hard proton collision
at fixed target experiments with a particular attention carried to the SHiP
facility.</p>
<div class="section" id="motivations">
<h2>Motivations<a class="headerlink" href="#motivations" title="Permalink to this headline">¶</a></h2>
<p>In the quest for Dark Matter, a new possibility of collider production (SHiP,
NA64, FASER, SeaQuest,…) has emerged as very promising. It is hypothesized
that the yet non-observation of dark matter is due to its tiny interaction
strength. In this paradigm one possible scenario predicts the existence of
vector portals, so-called dark photons.</p>
<p>In these fixed target experiments, three dark photon production channels need
to be considered: meson decay, Bremstrahlung and Drell-Yan process, each
dominant for increasing masses. This project provides a fast method for
computing the hard production (Drell-Yan) at next-to-leading order.</p>
<div class="figure align-default">
<img alt="_images/setup.png" src="_images/setup.png" />
</div>
<p>Theoretical details and implementation description can be found in the paper TODO.</p>
</div>
<div class="section" id="description">
<h2>Description<a class="headerlink" href="#description" title="Permalink to this headline">¶</a></h2>
<p>The leading order (LO) approximation of the Drell-Yan production of dark photons
can efficiently be computed with the Naive Parton Model (NPM). However,
next-to-leading order (NLO) corrections are called for since:</p>
<ul class="simple">
<li><p>they lead to a correction of order one, and</p></li>
<li><p>they provide information on the transverse momentum profile lacking at LO.</p></li>
</ul>
<p>When considering the NLO computation one encounters IR divergences. The
collinear ones are accounted for by the Parton Distribution Functions (PDFs).
The soft ones cancel at large transverse momentum (<span class="math notranslate nohighlight">\(k_T &gt; \Lambda_{QCD}\)</span>)
and for the integrated cross section, but must be resummed at small transverse
momentum (<span class="math notranslate nohighlight">\(k_T &lt; \Lambda_{QCD}\)</span>). The resummation formalism proposed by
Collins, Soper and Sterman (CSS) allows to reorganize the logarithm divergences
in a Sudakov factor. The NLO precision is reached by considering terms up to the
next-to-leading logarithm (NLL).</p>
<p>When matching the large and small <span class="math notranslate nohighlight">\(k_T\)</span> predictions the divergent part of
the NLO prediction is needed in principle. It is defined by retaining all terms
proportional to <span class="math notranslate nohighlight">\((1 + \text{ logs of } k_T) / k_T^2\)</span> or
<span class="math notranslate nohighlight">\(\delta(k_T)\)</span>. The actual differential cross section is then</p>
<div class="math notranslate nohighlight">
\[d\sigma = d\sigma_{CSS} + d\sigma_{NLO} - d\sigma_{DIV}\]</div>
<p>In practice, because of phenomenological corrections, <span class="math notranslate nohighlight">\(d\sigma_{NLO}\)</span> and
<span class="math notranslate nohighlight">\(d\sigma_{DIV}\)</span> don’t cancel exactly at large <span class="math notranslate nohighlight">\(k_T\)</span> which motivates
the computation of the following inferior and superior bounds instead</p>
<div class="math notranslate nohighlight">
\[\begin{split}d\sigma_{inf} =
\begin{cases}
d\sigma_{CSS} &amp; \text{, if  } k_T &lt; k_{T1} \\
d\sigma_{NLO} &amp; \text{, if  } k_T &gt; k_{T1}
\end{cases}\end{split}\]</div>
<div class="math notranslate nohighlight">
\[\begin{split}d\sigma_{sup} =
\begin{cases}
d\sigma_{CSS} &amp; \text{, if  } k_T &lt; k_{T2} \\
d\sigma_{NLO} &amp; \text{, if  } k_T &gt; k_{T2}
\end{cases}\end{split}\]</div>
<p>with the transition momenta <span class="math notranslate nohighlight">\(k_{T1}\)</span> and <span class="math notranslate nohighlight">\(k_{T2}\)</span> respectively
defined as the momenta of first and second intersection between the NLO and CSS
predictions.</p>
</div>
<div class="section" id="package-structure">
<h2>Package structure<a class="headerlink" href="#package-structure" title="Permalink to this headline">¶</a></h2>
<p>The <em>DarkPhotonSimulator</em> package is two fold.</p>
<ul>
<li><p>The first part (<em>dps.generation</em>) is concerned with the implementation of the
different predictions for the differential and integrated cross section at LO
and NLO, together with their scale and PDF uncertainties. The table below
summarize the available quantities.</p>
<div class="figure align-default">
<a class="reference internal image-reference" href="_images/table.png"><img alt="_images/table.png" src="_images/table.png" style="width: 384.0px; height: 313.6px;" /></a>
</div>
<p>Any fixed target experiment can be used in principle, but the SHiP facility is
used by default. See TODO to change the beam energy.</p>
</li>
<li><p>The second part (<em>dps.fit</em>) is dedicated to speeding up the computation. The
objective is achieved by fitting data computed with the first part and then
interpolating for any mass in the range. The above described matching
procedure is then implemented. Fits of the NPM and NLO integrated cross
section are also provided.</p>
<p>Cross section data associated to the SHiP facility is readily available for
masses in the range <span class="math notranslate nohighlight">\([1\:GeV, 12\:GeV]\)</span>.</p>
</li>
</ul>
<p>Modules describing the experimental setup (<em>dps.setup</em>), the dark photons
kinematic properties (<em>dps.darkphoton</em>) or to generate momenta according to a
chosen distribution (<em>dps.sample</em>) are shared by the two parts.</p>
</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">darkphoton simulator</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Overview</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#motivations">Motivations</a></li>
<li class="toctree-l2"><a class="reference internal" href="#description">Description</a></li>
<li class="toctree-l2"><a class="reference internal" href="#package-structure">Package structure</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="dps.html">DarkPhotonSimulator</a></li>
<li class="toctree-l1"><a class="reference internal" href="usage.html">Usage</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">Welcome to DarkPhotonSimulator’s documentation!</a></li>
      <li>Next: <a href="dps.html" title="next chapter">DarkPhotonSimulator</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2019, Jonas Paccolat.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 2.1.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/overview.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>
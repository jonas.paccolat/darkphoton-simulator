========
Overview
========

This package simulates the production of dark photons in hard proton collision
at fixed target experiments with a particular attention carried to the SHiP
facility.

Motivations
-----------

In the quest for Dark Matter, a new possibility of collider production (SHiP,
NA64, FASER, SeaQuest,...) has emerged as very promising. It is hypothesized
that the yet non-observation of dark matter is due to its tiny interaction
strength. In this paradigm one possible scenario predicts the existence of
vector portals, so-called dark photons.

In these fixed target experiments, three dark photon production channels need
to be considered: meson decay, Bremstrahlung and Drell-Yan process, each
dominant for increasing masses. This project provides a fast method for
computing the hard production (Drell-Yan) at next-to-leading order.

.. figure:: figures/setup.png

Theoretical details and implementation description can be found in the paper TODO.

Description
-----------

The leading order (LO) approximation of the Drell-Yan production of dark photons
can efficiently be computed with the Naive Parton Model (NPM). However,
next-to-leading order (NLO) corrections are called for since:

* they lead to a correction of order one, and
* they provide information on the transverse momentum profile lacking at LO.

When considering the NLO computation one encounters IR divergences. The
collinear ones are accounted for by the Parton Distribution Functions (PDFs).
The soft ones cancel at large transverse momentum (:math:`k_T > \Lambda_{QCD}`)
and for the integrated cross section, but must be resummed at small transverse
momentum (:math:`k_T < \Lambda_{QCD}`). The resummation formalism proposed by
Collins, Soper and Sterman (CSS) allows to reorganize the logarithm divergences
in a Sudakov factor. The NLO precision is reached by considering terms up to the
next-to-leading logarithm (NLL).

When matching the large and small :math:`k_T` predictions the divergent part of
the NLO prediction is needed in principle. It is defined by retaining all terms
proportional to :math:`(1 + \text{ logs of } k_T) / k_T^2` or
:math:`\delta(k_T)`. The actual differential cross section is then

.. math::

  d\sigma = d\sigma_{CSS} + d\sigma_{NLO} - d\sigma_{DIV}

In practice, because of phenomenological corrections, :math:`d\sigma_{NLO}` and
:math:`d\sigma_{DIV}` don't cancel exactly at large :math:`k_T` which motivates
the computation of the following inferior and superior bounds instead

.. math::

  d\sigma_{inf} =
  \begin{cases}
  d\sigma_{CSS} & \text{, if  } k_T < k_{T1} \\
  d\sigma_{NLO} & \text{, if  } k_T > k_{T1}
  \end{cases}

.. math::

  d\sigma_{sup} =
  \begin{cases}
  d\sigma_{CSS} & \text{, if  } k_T < k_{T2} \\
  d\sigma_{NLO} & \text{, if  } k_T > k_{T2}
  \end{cases}

with the transition momenta :math:`k_{T1}` and :math:`k_{T2}` respectively
defined as the momenta of first and second intersection between the NLO and CSS
predictions.

Package structure
-----------------

The *DarkPhotonSimulator* package is two fold.

* The first part (*dps.generation*) is concerned with the implementation of the
  different predictions for the differential and integrated cross section at LO
  and NLO, together with their scale and PDF uncertainties. The table below
  summarize the available quantities.

  .. figure:: figures/table.png
    :scale: 80%

  Any fixed target experiment can be used in principle, but the SHiP facility is
  used by default. See TODO to change the beam energy.

* The second part (*dps.fit*) is dedicated to speeding up the computation. The
  objective is achieved by fitting data computed with the first part and then
  interpolating for any mass in the range. The above described matching
  procedure is then implemented. Fits of the NPM and NLO integrated cross
  section are also provided.

  Cross section data associated to the SHiP facility is readily available for
  masses in the range :math:`[1\:GeV, 12\:GeV]`.

Modules describing the experimental setup (*dps.setup*), the dark photons
kinematic properties (*dps.darkphoton*) or to generate momenta according to a
chosen distribution (*dps.sample*) are shared by the two parts.

# -*- coding: utf-8 -*-

import os
import glob
import pickle
import numpy as np

from dps.logs import log

def get_index(array):
    """
    Sort the indices of the given array to acces all identical values.

    Parameters
    ----------
    array: array-like
        List of values to sort.

    Returns
    -------
    dict:
        The *i*st label of the ouput is the *i*st smallest value in *array* and
        the *j*st component of the corresponding list is the index of the *j*st
        apparition of this value in *array*.
    """

    labels = list(set(array))
    labels.sort()

    index = {label: list() for label in labels}
    for i, x in enumerate(array):
        index[x].append(i)

    return index

def get_max_depth(index):
    """
    Find the size of the longest list from the dictionnary of lists *index*.

    Parameters
    ----------
    index: dict
        Dictionnary of list of indices.

    Returns
    -------
    int:
        Size of the longest list of indices.
    """

    depths = list()
    for x in index.values():
        depths.append(len(x))

    return max(depths)

def reshape(res, axis=0):
    """
    Reshape the differential cross section 1-arrays into 2-arrays with *z* and
    *kT* axes.

    Parameters
    ----------
    res: dict
        Dictionnary of the double differential cross section generated with
        *ddcs* with keys: *z*, *kT*, *css*, *nlo* and *div*, each storing a
        1d-array.
    axis: int, optional
        The first index corresponds to a cut along *z* if *axis=0* and along
        *kT* if *axis=1*. Default is 0.

    Returns
    -------
    Dictionnary of the differential cross sections and coordinates stored in
    2d-arrays with *z* and *kT* axes.
    """

    if axis == 0:
        index = get_index(res["kT"])
    else:
        index = get_index(res["z"])

    n_col = len(index)
    n_row = get_max_depth(index)

    data = dict()
    for key in res.keys():
        data[key] = np.zeros((n_col, n_row))
        for i, idx in enumerate(index.values()):
            data[key][i][:len(idx)] = res[key][idx]

    return data

def sort(dir):
    """
    Load all the .pkl files in the directory *dir* and sort them in increasing
    mass order.

    Parameters
    ----------
    dir: str
        Relative path to directory from script location.

    Returns
    -------
    args: list
        List of Namespaces in increasing mass order.
    res: list
        List of the cross section dictionnaries in increasing mass order.
    """

    here = os.path.dirname(__file__)
    path = os.path.join(here, dir)

    args = list()
    res = list()
    for filename in glob.glob('{}/*.pkl'.format(path)):
        with open(filename, "rb") as f:
            args.append(pickle.load(f))
            res.append(pickle.load(f))

    masses = [a.M for a in args]
    index = np.argsort(masses)

    args = [args[i] for i in index]
    res = [res[i] for i in index]

    return args, res

def remove_singularity(res):
    """
    Replace the differential cross section at *kT=0*, by the one at the next
    *kT* value.

    This trick is used to get rid of the divergence at *kT=0* for the NLO and
    DIV predicitons.

    Parameters
    ----------
    data: dataframe
        Dictionnary storing the differential cross section.

    Returns
    -------
    dataframe: Updated dictionnary.
    """

    index = get_index(res["z"])

    for key in ["nlo", "div"]:

        for i1, x in enumerate(res[key]):
            if np.isnan(x):
                for idx in index.values():
                    for j, i2 in enumerate(idx):
                        if i1 == i2:
                            try:
                                res[key][i1] = res[key][idx[j+1]]
                            except IndexError:
                                res[key][i1] = 1.
                    else:
                        continue
                    break

    return res

def clean_data(dir, subdir):
    """
    Clean all the data in the directory *dir* and save it in *subdir*.

    Parameters
    ----------
    dir: str
        Relative path to the data directory from "dps/data/".
    subdir: str
        Name of the subdirectory created in *dir* to save the cleaned data.
    """

    here = os.path.dirname(__file__)
    path = os.path.join(here, dir)

    subpath = os.path.join(path, subdir)

    try:
        os.mkdir(subpath)
    except OSError:
        print ("Creation of the directory %s failed" % subpath)

    args = list()
    res = list()
    for filename in glob.glob('{}/*.pkl'.format(path)):
        with open(filename, "rb") as f:
            args = pickle.load(f)
            res = pickle.load(f)

        remove_singularity(res)
        with open("{}/{}".format(subpath, os.path.basename(filename)), "wb") as g:
            args.clean = True
            pickle.dump(args, g)
            pickle.dump(res, g)

        log.info("{} cleaned.".format(os.path.basename(filename)))

def is_clean(dir):
    """
    Check wether the data in the directory *dir* has already been cleaned.

    Print the name of the uncleaned data.

    Parameters
    ----------
    dir: str
        Relative path to the data directory from "dps/data/".
    """

    here = os.path.dirname(__file__)
    path = os.path.join(here, dir)

    args = list()
    res = list()
    for filename in glob.glob('{}/*.pkl'.format(path)):
        with open(filename, "rb") as f:
            args = pickle.load(f)
            try:
                args.clean
                pass
            except:
                text = "{} not clean. It may lead to bad fit. Use sort.clean_data."
                log.warning(text.format(os.path.basename(filename)))

# -*- coding: utf-8 -*-

import lhapdf
from dps.logs import log

class PDF:

    def __init__(self, pdfset):
        self.pdfset = pdfset
        self.alpha = lhapdf.mkAlphaS(self.pdfset)
        self.pset = lhapdf.getPDFSet(self.pdfset)
        self.pdfs = self.pset.mkPDFs()
        self.pcentral = self.pset.mkPDF(0)
        self.q2Min = self.pcentral.q2Min

    def pdf(self, xF, iParton, Q2, mem=0):
        """
        Returns the PDF value.

        Parameters
        ----------
        xF: float
            Longitudinal momentum fraction. It must lie between 0 and 1.
        iParton: int
            Parton type according to the following code: 0=gluon, 1=down, 2=up,
            3=strange, 4=charm, 5=bottom, 6=top charges and the associated
            antiparticles for the opposite numbers.
        Q2: float
            Energy scale squared.
        mem: int, optional
            PDF set member. Default is 0.

        Returns
        -------
        float
            Value of the PDF for the considered variables.
        """

        if xF < 0:
            text = 'PDF longitudinal fraction out of range: xF={} < 0.'
            log.debug(text.format(xF))
            return 0.0
        elif xF > 1:
            text = 'PDF longitudinal fraction out of range: xF={} > 0.'
            log.debug(text.format(xF))
            return 0.0
        else:
            return self.pdfs[mem].xfxQ2(iParton, xF, Q2) / xF

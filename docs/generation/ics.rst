==================
dps.generation.ics
==================

Module to compute the integrated cross section predictions over a given mass
range.

Functions
---------

.. automodule:: dps.generation.ics
	:members:
	:private-members:

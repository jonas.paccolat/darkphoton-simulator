==============
dps.darkphoton
==============

Module defining the class *DarkPhoton*, which defines a dark photon of fixed
mass. It can be used to access the kinematic properties of a dark photon as well
as the physical domain boundaries.

If the longitudinal fraction(s) and the transverse momentum(a) are provided, the
kinematic variables attribute *yCM*, *xA*, *xB*, *MT*, *kL*, *Ek* can be set by
calling *set_kinematics()*.

Class
-----

.. autoclass:: dps.darkphoton.DarkPhoton
	:members:
	:private-members:

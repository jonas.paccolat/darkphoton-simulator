===================
dps.generation.sdcs
===================

Module to cover the single differential cross section domain and compute its
different predictions.

Functions
---------

.. automodule:: dps.generation.sdcs
	:members:
	:private-members:

==================
dps.generation.css
==================

Implementation of the CSS prediction of the differential cross section.

Functions
---------

.. automodule:: dps.generation.css
	:members:
	:private-members:

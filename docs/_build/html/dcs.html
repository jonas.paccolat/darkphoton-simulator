
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>dps.fit.dcs &#8212; darkphoton simulator 1.0.0 documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/language_data.js"></script>
    <script async="async" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="dps.fit.ics" href="ics.html" />
    <link rel="prev" title="dps.fit" href="fit.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="dps-fit-dcs">
<h1>dps.fit.dcs<a class="headerlink" href="#dps-fit-dcs" title="Permalink to this headline">¶</a></h1>
<p>In this module the fit class <em>DCS</em> is defined. Once instantiated for a given
mass it allows a fast computation of the differential cross section. It is most
efficient for arrays of more than <span class="math notranslate nohighlight">\(10^6\)</span> momenta where it takes
<span class="math notranslate nohighlight">\(10^{-5} \: s\)</span> per momentum.</p>
<p>The actual physical domain is slightly reduced because of fitting instabilities
in regions of suppressed differential cross section (gray domain on the left
figure).</p>
<p>Two procedures are implemented for the matching between the small and large
<span class="math notranslate nohighlight">\(k_T\)</span> regimes giving a superior and inferior bound on the differential
cross section. The superior / inferior bound (<em>sup</em> / <em>inf</em>) is obtained by
considering the <em>css</em> / <em>nlo</em> prediction in the transition region (blue domain
on the left figure). The transition lines <em>kT1</em> and <em>kT2</em> are defined as the
transverse momenta where the <em>css</em> domination over the <em>nlo</em> starts (first
intersection) and respectively where it ends (second intersection), as pictured
on the right figure.</p>
<div class="figure align-default">
<img alt="_images/domain.png" src="_images/domain.png" />
</div>
<div class="section" id="class">
<h2>Class<a class="headerlink" href="#class" title="Permalink to this headline">¶</a></h2>
<dl class="class">
<dt id="dps.fit.dcs.DCS">
<em class="property">class </em><code class="sig-prename descclassname">dps.fit.dcs.</code><code class="sig-name descname">DCS</code><span class="sig-paren">(</span><em class="sig-param">M</em>, <em class="sig-param">Nz=300</em>, <em class="sig-param">NkT=300</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS" title="Permalink to this definition">¶</a></dt>
<dd><p>Class to generate fitted fixed mass dark photons.</p>
<p>Any mass in the range [1 GeV, 12 GeV] can be extrapolated from the data.
The predictions from the <em>css</em>, <em>nlo</em> and <em>div</em> methods are implemented
first and used to define the reliable fitting domain as well as the
transition curves separating the different regimes. Two matching procedures
are implemented leading to an upper bound (<em>sup</em>) and a lower bound (<em>inf</em>)
on the differential cross section.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>M</strong> (<em>float</em>) – Mass of the dark photons (in [GeV]).</p></li>
<li><p><strong>Nz</strong> (<em>int</em><em>, </em><em>optional</em>) – Number of points along <em>z</em> for the interpolation. Default is 300.</p></li>
<li><p><strong>NkT</strong> (<em>int</em><em>, </em><em>optional</em>) – Number of points along <em>kT</em> for the interpolation. Default is 300.</p></li>
</ul>
</dd>
</dl>
<dl class="method">
<dt id="dps.fit.dcs.DCS.css">
<code class="sig-name descname">css</code><span class="sig-paren">(</span><em class="sig-param">z</em>, <em class="sig-param">kT</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.css"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.css" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the <em>css</em> differential cross section for dark photons of
coordinates <em>z</em> and <em>kT</em>.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>z</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Longitudinal fractions of the dark photons.</p></li>
<li><p><strong>kT</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Transverse momentum of the dark photons (in [GeV]).</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><em>css</em> prediction of the differential cross section.</p>
</dd>
<dt class="field-odd">Return type</dt>
<dd class="field-odd"><p>(.., N, M) ndarray</p>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.div">
<code class="sig-name descname">div</code><span class="sig-paren">(</span><em class="sig-param">z</em>, <em class="sig-param">kT</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.div"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.div" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the <em>div</em> differential cross section for dark photons of
coordinates <em>z</em> and <em>kT</em>.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>z</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Longitudinal fractions of the dark photons.</p></li>
<li><p><strong>kT</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Transverse momentum of the dark photons (in [GeV]).</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><em>div</em> prediction of the differential cross section.</p>
</dd>
<dt class="field-odd">Return type</dt>
<dd class="field-odd"><p>(.., N, M) ndarray</p>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.find_kTlimits">
<code class="sig-name descname">find_kTlimits</code><span class="sig-paren">(</span><em class="sig-param">css</em>, <em class="sig-param">nlo</em>, <em class="sig-param">kT</em>, <em class="sig-param">realkTmax</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.find_kTlimits"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.find_kTlimits" title="Permalink to this definition">¶</a></dt>
<dd><p>Find the kT limits (kT1, kT2 and kTmax) for a fixed z.</p>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.fit">
<code class="sig-name descname">fit</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.fit"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.fit" title="Permalink to this definition">¶</a></dt>
<dd><p>Set the <em>css</em>, <em>nlo</em> and <em>div</em> predictions for the class mass.</p>
<p>The data at masses encapsulating the class mass are first fitted on the
(z, kT)-plane before being interpolated to the class mass.</p>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.in_domain">
<code class="sig-name descname">in_domain</code><span class="sig-paren">(</span><em class="sig-param">z</em>, <em class="sig-param">kT</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.in_domain"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.in_domain" title="Permalink to this definition">¶</a></dt>
<dd><p>Boolean function checking that (<em>z</em>, <em>kT</em>) belongs to the reliable
region.</p>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.inf">
<code class="sig-name descname">inf</code><span class="sig-paren">(</span><em class="sig-param">z</em>, <em class="sig-param">kT</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.inf"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.inf" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the <em>inf</em> differential cross section for dark photons of
coordinates <em>z</em> and <em>kT</em>.</p>
<p>The <em>inf</em> differential cross section is defined as <em>inf=css</em> if <em>kT&lt;kT2</em>
and <em>inf=nlo</em> if <em>kT&gt;kT2</em>.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>z</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Longitudinal fractions of the dark photons.</p></li>
<li><p><strong>kT</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Transverse momentum of the dark photons (in [GeV]).</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><em>inf</em> prediction of the differential cross section.</p>
</dd>
<dt class="field-odd">Return type</dt>
<dd class="field-odd"><p>(.., N, M) ndarray</p>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.information_loss">
<code class="sig-name descname">information_loss</code><span class="sig-paren">(</span><em class="sig-param">Nz=1000</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.information_loss"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.information_loss" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute an upper bound of the ratio between the integration of the
differential cross section outside and inside of the reliable region
for the <em>inf</em> and the <em>sup</em> schemes.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><p><strong>Nz</strong> (<em>int</em><em>, </em><em>optional</em>) – Number of points along <em>z</em>. Default is 1000.</p>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><ul class="simple">
<li><p><strong>r_inf</strong> (<em>float</em>) – Inferior ratio.</p></li>
<li><p><strong>r_sup</strong> (<em>float</em>) – Superior ratio.</p></li>
</ul>
</p>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.init_darkphoton">
<code class="sig-name descname">init_darkphoton</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.init_darkphoton"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.init_darkphoton" title="Permalink to this definition">¶</a></dt>
<dd><p>Instantiate a dark photon of the class mass.</p>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.integrate">
<code class="sig-name descname">integrate</code><span class="sig-paren">(</span><em class="sig-param">Nz=500</em>, <em class="sig-param">NkT=5000</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.integrate"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.integrate" title="Permalink to this definition">¶</a></dt>
<dd><p>Integrate the <em>inf</em> and <em>sup</em> differential cross section over the
whole reliable region.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>Nz</strong> (<em>int</em><em>, </em><em>optional</em>) – Number of points along <em>z</em>. Default is 500.</p></li>
<li><p><strong>NkT</strong> (<em>int</em><em>, </em><em>optional</em>) – Number of points along <em>kT</em>. Default is 5000.</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><ul class="simple">
<li><p><strong>cs_inf</strong> (<em>float</em>) – Inferior integrated cross section.</p></li>
<li><p><strong>cs_sup</strong> (<em>float</em>) – Superior integrated cross section.</p></li>
</ul>
</p>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.limit_functions">
<code class="sig-name descname">limit_functions</code><span class="sig-paren">(</span><em class="sig-param">z</em>, <em class="sig-param">kTmax</em>, <em class="sig-param">kT1</em>, <em class="sig-param">kT2</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.limit_functions"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.limit_functions" title="Permalink to this definition">¶</a></dt>
<dd><p>Smoothen and interpolate the kT limit curves.</p>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.load_dataset">
<code class="sig-name descname">load_dataset</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.load_dataset"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.load_dataset" title="Permalink to this definition">¶</a></dt>
<dd><p>Load the differential cross section data for all masses.</p>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.nlo">
<code class="sig-name descname">nlo</code><span class="sig-paren">(</span><em class="sig-param">z</em>, <em class="sig-param">kT</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.nlo"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.nlo" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the <em>nlo</em> differential cross section for dark photons of
coordinates <em>z</em> and <em>kT</em>.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>z</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Longitudinal fractions of the dark photons.</p></li>
<li><p><strong>kT</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Transverse momentum of the dark photons (in [GeV]).</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><em>nlo</em> prediction of the differential cross section.</p>
</dd>
<dt class="field-odd">Return type</dt>
<dd class="field-odd"><p>(.., N, M) ndarray</p>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.set_kTlimits">
<code class="sig-name descname">set_kTlimits</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.set_kTlimits"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.set_kTlimits" title="Permalink to this definition">¶</a></dt>
<dd><p>Set the regime transition curves (<em>kT1(z)</em> and <em>kT2(z)</em>) and the
reliable domain limit curve (<em>kTmax(z)</em>).</p>
</dd></dl>

<dl class="method">
<dt id="dps.fit.dcs.DCS.sup">
<code class="sig-name descname">sup</code><span class="sig-paren">(</span><em class="sig-param">z</em>, <em class="sig-param">kT</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dps/fit/dcs.html#DCS.sup"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dps.fit.dcs.DCS.sup" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the <em>sup</em> differential cross section for dark photons of
coordinates <em>z</em> and <em>kT</em>.</p>
<p>The <em>sup</em> differential cross section is defined as <em>sup=css</em> if <em>kT&lt;kT2</em>
and <em>sup=nlo</em> if <em>kT&gt;kT2</em>.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>z</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Longitudinal fractions of the dark photons.</p></li>
<li><p><strong>kT</strong> (<em>(</em><em>..</em><em>, </em><em>N</em><em>, </em><em>M</em><em>) </em><em>array_like</em>) – Transverse momentum of the dark photons (in [GeV]).</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><em>sup</em> prediction of the differential cross section.</p>
</dd>
<dt class="field-odd">Return type</dt>
<dd class="field-odd"><p>(.., N, M) ndarray</p>
</dd>
</dl>
</dd></dl>

</dd></dl>

</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">darkphoton simulator</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="dps.html">DarkPhotonSimulator</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="darkphoton.html">dps.darkphoton</a></li>
<li class="toctree-l2"><a class="reference internal" href="data.html">dps.data</a></li>
<li class="toctree-l2"><a class="reference internal" href="distribution.html">dps.distribution</a></li>
<li class="toctree-l2 current"><a class="reference internal" href="fit.html">dps.fit</a></li>
<li class="toctree-l2"><a class="reference internal" href="generation.html">dps.generation</a></li>
<li class="toctree-l2"><a class="reference internal" href="sample.html">dps.sample</a></li>
<li class="toctree-l2"><a class="reference internal" href="setup.html">dps.setup</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="usage.html">Usage</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
  <li><a href="dps.html">DarkPhotonSimulator</a><ul>
  <li><a href="fit.html">dps.fit</a><ul>
      <li>Previous: <a href="fit.html" title="previous chapter">dps.fit</a></li>
      <li>Next: <a href="ics.html" title="next chapter">dps.fit.ics</a></li>
  </ul></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2019, Jonas Paccolat.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 2.1.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/dcs.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>
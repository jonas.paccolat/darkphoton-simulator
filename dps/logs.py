# -*- coding: utf-8 -*-

import logging
import sys

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

formatter = logging.Formatter(fmt="%(filename)s line %(lineno)s - %(levelname)s: %(message)s")

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)

fh = logging.FileHandler("logs.log", "w+")
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)

log.addHandler(ch)
log.addHandler(fh)

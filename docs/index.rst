Welcome to DarkPhotonSimulator's documentation!
================================================

DarkPhotonSimulator is a python package for efficient numeric simulation of dark
photons generated at fixed target proton experiments.

.. toctree::
   :maxdepth: 2

   overview
   dps
   usage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

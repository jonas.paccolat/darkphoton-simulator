===========
dps.fit.ics
===========

In this module the fit class ICS is defined. It allows to compute the integrated
cross section for any mass in the range :math:`[1\:GeV, 12\:GeV]`.

The Naive Parton Model (NPM) prediction as well as the next-to-leading order
(NLO) predictions may be accessed.

The uncertainties arising from the PDF variability are computed for the CT14nlo
PDFset with a confidence level of 95%.

The uncertainties arising from the choice of the factorization scale are
estimated by varying the the scale between :math:`\\mu_F=M/2` and
:math:`\\mu_F=2M`, where :math:`M` is the dark photon mass.

Class
-----

.. autoclass:: dps.fit.ics.ICS
	:members:
	:private-members:

=====
Usage
=====

Installation
------------

The package can be installed locally by first downloading the source code from
https://gitlab.com/jonas.paccolat/ship-darkphoton-simulator and then running

.. code-block:: bash

  python setup.py install

Dependencies
------------

In order to use the subpackage *dps.generation* one needs to install the LHAPDF
library for python 3. This can be achieved by first installing a python 3
virtual environment and then following the instruction from
https://lhapdf.hepforge.org/install.html

The PDF set *CT14nlo* is used by default and should be downloaded from `PDF list
<https://lhapdf.hepforge.org/pdfsets.html>`_ into the folder
*/lhapdf/share/LHAPDF/*. To use an other PDF set, please refer to
:ref:`default_parameters`.

Compute cross sections
----------------------

All cross sections can be computed with the command line

.. code-block:: bash

  gen

with arguments

* ``--d``: (required) Cross section type. 0: integrated, 1: single differential.,
  2: double differential.
* ``--u``: (default 0) Uncertainty. 0: No, 1: Yes.
* ``--M``: Dark photon mass in [GeV] (d=1,2).
* ``--Mmin``: Smallest dark photon mass [GeV] (d=0).
* ``--Mmax``: Largest dark photon mass [GeV] (d=0).
* ``--mesh``: Coordinate gradation type. "constant" (d=1,2), "gaussian" (d=1,2),
  "random" (d=2), "linear" (d=0) or "log" (d=0).
* ``--N``: Sample size (d=0,1,2).
* ``--Nz``: Sample size along z (d=2).
* ``--NkT``: Sample size along kT (d=2).
* ``--seed``: Random seed for random sample (u=1).
* ``--pickle``: (required) Path to output.

In order to run multiple jobs in parallel the script *nrun.py*
(https://github.com/mariogeiger/grid) can be called as

.. code-block:: bash

  python nrun.py --n 3 output "gen --d 2 --Nz 100 --NkT 100 --mesh gaussian" --M 1 2 3

In that case, 3 processes would run in parallel to compute the double
differential cross section at M=1, 2 and 3 GeV. The data files are saved with
random names together with their arguments in the directory *output*.

One can then easily load the data from a python shell

.. code-block:: python

  import pickle
  with open(filename, "rb") as f:
    args = pickle.load(f)
    res = pickle.load(f)


Fit and generate data
---------------------

Import the package modules:

.. code-block:: python

  from dps import darkphoton, sample
  from dps.fit import dcs, ics

Define a dark photon:

.. code-block:: python

  # create a dark photon instance of mass 2.6 GeV
  dp = darkphoton.DarkPhoton(2.6)

  # set 100000 random momenta to the dark photon
  sample.sample(dp, "random", 100000, seed=0, set_coord=True, quiet=True)
  # more sampling options are described in the documentation.

Compute the differential cross section (of the dark photon momenta):

.. code-block:: python

  # fit instance for the dark photon mass
  d = dcs.DCS(dp.M)

  # inferior and superior predictions for the dark photon momenta
  inf = d.inf(dp.z, dp.kT)
  sup = d.sup(dp.z, dp.kT)

Compute the integrated cross section:

.. code-block:: python

  # fit instance
  i = ics.ICS()

  # NPM and NLO predictions
  nlo = i.nlo("central", dp.M)
  npm = i.npm("central", dp.M)

  # PDF uncertainties
  nlo_pdf = i.nlo("pdf", dp.M)
  npm_pdf = i.npm("pdf", dp.M)

  # scale uncertainties
  nlo_scale = i.nlo("scale", dp.M)
  npm_scale = i.npm("scale", dp.M)

.. _default_parameters:

Default parameters
------------------

Some users may want to modify the following default parameters:

* The energy of the proton beam and the number of QCD active flavors can be
  modified in the file */dps/setup.py*.
* In order to use a different PDF set, the name of the new PDF set must be
  specified in the darkphoton instanciation in the heading of the files
  */dps/generation/css.py*, */dps/generation/nlo.py*, */dps/generation/div.py*
  and */dps/generation/npm.py*. The associated PDF set must be downloaded into
  the folder */lhapdf/share/LHAPDF/*.
* By default the factorization scale is set to the dark photon mass. This
  default value may be modified by changing the attribute *scale* in the methods
  *differential_cross_section* and *integrated_cross_section* of the files
  */dps/generation/nlo.py*, */dps/generation/div.py* and */dps/generation/npm.py*.
* The BLNY parametrization is used by default for the non-perturbative
  corrections of the CSS integral. An other parametrization can be implemented
  by adding it in the method *init_para* in the file */dps/generation/css.py*
  and referring to it in the calls to *css.differential_cross_section* and
  *css.integrated_cross_section* in the file */dps/generation/ddcs.py*.

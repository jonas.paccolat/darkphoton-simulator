==================
dps.generation.npm
==================

Implementation of the NPM prediction of the differential cross section.

Functions
---------

.. automodule:: dps.generation.npm
	:members:
	:private-members:

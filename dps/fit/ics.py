# -*- coding: utf-8 -*-

import os
import pickle
import copy
import numpy as np
from scipy.interpolate import interp1d

from dps.fit import interpolate
from dps import darkphoton
from dps import sample
from dps.logs import log

class ICS(object):
    """
    Class to compute the integrated cross section. Both NPM and NLO predictions
    are implemented.

    The scale and PDF uncertainties are also available.
    """

    def __init__(self):
        self.central, self.uncertainty = self.load()
        self.Mmin, self.Mmax = self.get_Mmin_Mmax()
        self.fits = self.fit()

    def load(self):
        """
        Load the central and uncertainty prediciton for both NPM and NLO.
        """

        input = "../data/ics/"

        here = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(here, input, "central.pkl"), "rb") as f:
            args = pickle.load(f)
            central = pickle.load(f)

        with open(os.path.join(here, input, "uncertainty.pkl"), "rb") as f:
            args = pickle.load(f)
            uncertainty = pickle.load(f)

        return central, uncertainty

    def get_Mmin_Mmax(self):
        """
        Get the inferior and superior bounds of the mass range.
        """

        Mmin = self.central["M"][0]
        Mmax = self.central["M"][-1]

        return Mmin, Mmax

    def fit(self):
        """
        Fit the central (expected) and uncertainty predictions for both NPM and
        NLO.
        """

        fits = {"nlo": dict(), "npm": dict()}

        for key1 in ["nlo", "npm"]:

            fits[key1]["central"] = interp1d(self.central["M"], self.central[key1], kind='quadratic', bounds_error=False)

            for key2 in ["pdf", "scale"]:

                ics1 = np.array([x for x, y in self.uncertainty[key1][key2]])
                ics2 = np.array([y for x, y in self.uncertainty[key1][key2]])

                f1 = interp1d(self.uncertainty['M'], ics1, kind='quadratic', bounds_error=False)
                f2 = interp1d(self.uncertainty['M'], ics2, kind='quadratic', bounds_error=False)

                def f(M, f1=f1, f2=f2):
                    return f1(M), f2(M)

                fits[key1][key2] = f

        return fits

    def npm(self, kind, M):
        """
        Compute the NPM prediciton for the integrated cross section of a dark
        photon of mass *M*.

        The expected cross section and its uncertainty interval can be returned.

        Parameters
        ----------
        kind: str
            "central" for the expected integrated cross section, or "pdf" for
            the PDF uncertainty interval, or "scale" for the scale uncertainty
            interval.
        M: float or array-like
            Mass(es) of the dark photon in GeV. It must lie in the interval
            [1 GeV, 12 GeV].

        Returns
        -------
        ics: float or array-like
            Expected NPM integrated cross section in mb. For *kind* = "central".
        (ics_min, ics_max): (float, float) or (array-like, array-like)
            Uncertainty interval of the NPM integrated cross section in mb. For
            *kind* = "pdf" or "scale".
        """

        dcs = self.fits["npm"][kind](M)

        if type(M) == float or type(M) == int:
            dcs = dcs.item()

        return dcs

    def nlo(self, kind, M):
        """
        Compute the NLO prediciton for the integrated cross section of a dark
        photon of mass *M*.

        The expected cross section and its uncertainty interval can be returned.

        Parameters
        ----------
        kind: str
            "central" for the expected integrated cross section, or "pdf" for
            the PDF uncertainty interval, or "scale" for the scale uncertainty
            interval.
        M: float or array-like
            Mass(es) of the dark photon in GeV. It must lie in the interval
            [1 GeV, 12 GeV].

        Returns
        -------
        ics: float or array-like
            Expected NLO integrated cross section in mb. For *kind* = "central".
        (ics_min, ics_max): (float, float) or (array-like, array-like)
            Uncertainty interval of the NLO integrated cross section in mb. For
            *kind* = "pdf" or "scale".
        """

        dcs = self.fits["nlo"][kind](M)

        if type(M) == float or type(M) == int:
            dcs = dcs.item()

        return dcs

==========
dps.sample
==========

List of functions to generate fixed mass dark photon momenta within a given
region (physical domain or cut) following a given distribution.

Functions
---------

.. automodule:: dps.sample
	:members:
	:private-members:

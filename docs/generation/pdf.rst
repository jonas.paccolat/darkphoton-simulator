==================
dps.generation.pdf
==================

Class to access the PDF values and related quantities.

Class
-----

.. autoclass:: dps.generation.pdf.PDF
	:members:
	:private-members:

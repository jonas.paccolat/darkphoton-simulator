# -*- coding: utf-8 -*-

import numpy as np

from dps import setup
from dps.utils import plus_convolution, convolution
from dps.generation import pdf
from dps.logs import log

_pdf = pdf.PDF("CT14nlo")
_setup = setup.Setup()

def get_pdf():
    return _pdf

def F_quark(x):
    """One loop quark evolution kernel (to be convoluted)"""

    return (1 + x**2) / (1 - x)

def F_gluon(x):
    """One loop gluon evolution kernel (to be convoluted)"""

    return ((1 - x)**2 + x**2)

def G_quarkA(x, dp, iParton, fxA, fxB, Q2, mem):
    """Quark PDF terms of configuration A. (to be convoluted)"""

    # get +/- indces in the fxA basis
    ip = iParton + _setup.nf
    im = iParton - 1
    tot = fxA[ip] * _pdf.pdf(dp.xB / x, -iParton, Q2, mem) \
        + fxA[im] * _pdf.pdf(dp.xB / x, iParton, Q2, mem)

    return tot / x

def G_quarkB(x, dp, iParton, fxA, fxB, Q2, mem):
    """Quark PDF terms of configuration B. (to be convoluted)"""

    # get +/- indces in the fxB basis
    ip = iParton + _setup.nf
    im = iParton - 1
    tot = fxB[ip] * _pdf.pdf(dp.xA / x, -iParton, Q2, mem) \
        + fxB[im] * _pdf.pdf(dp.xA / x, iParton, Q2, mem)

    return tot / x

def G_gluonA(x, dp, iParton, fxA, fxB, Q2, mem):
    """Gluon PDF terms of configuration A. (to be convoluted)"""

    # get +/- indces in the fxA basis
    ip = iParton + _setup.nf
    im = iParton - 1
    tot = fxA[ip] * _pdf.pdf(dp.xB / x, 0, Q2, mem) \
        + fxA[im] * _pdf.pdf(dp.xB / x, 0, Q2, mem)

    return tot / x

def G_gluonB(x, dp, iParton, fxA, fxB, Q2, mem):
    """Gluon PDF terms of configuration B. (to be convoluted)"""

    # get +/- indces in the fxB basis
    ip = iParton + _setup.nf
    im = iParton - 1
    tot = fxB[ip] * _pdf.pdf(dp.xA / x, 0, Q2, mem) \
        + fxB[im] * _pdf.pdf(dp.xA / x, 0, Q2, mem)

    return tot / x

def get_fxA(dp, Q2, mem):
    """Get the PDF at xA for each parton."""

    fxA = [_pdf.pdf(dp.xA, iParton, Q2, mem) \
           for iParton in range(-_setup.nf, _setup.nf + 1)]

    return fxA

def get_fxB(dp, Q2, mem):
    """Get the PDF at the xB for each parton."""

    fxB = [_pdf.pdf(dp.xB, iParton, Q2, mem) \
           for iParton in range(-_setup.nf, _setup.nf + 1)]

    return fxB

def collinear_contribution(dp, fxA, fxB, Q2, mem, eps):
    """Collinear contribution to the divergent differential cross section."""

    tot = 0.
    for iParton in range(1, _setup.nf + 1):

        args = (dp, iParton, fxA, fxB, Q2, mem)
        tot += _setup.sigma0[iParton] \
             * ( \
                plus_convolution(F_quark, G_quarkA, dp.xB, args, eps=eps) \
              + plus_convolution(F_quark, G_quarkB, dp.xA, args, eps=eps) \
              + 3. / 8. * convolution(F_gluon, G_gluonA, dp.xB, args, eps=eps) \
              + 3. / 8. * convolution(F_gluon, G_gluonB, dp.xA, args, eps=eps)
                )

    return tot

def soft_contribution(dp, fxA, fxB):
    """Soft contribution to the divergent differential cross section."""

    tot = 0.
    for iParton in range(1, _setup.nf + 1):
        ip = iParton + _setup.nf
        im = iParton - 1
        tot += _setup.sigma0[iParton] \
             * (fxA[ip] * fxB[im] + fxA[im] * fxB[ip])
    tot *= 4. * np.log(dp.M / dp.kT) - 3.

    return tot

def differential_cross_section(dp, scale=1, mem=0, eps=1e-3):
    """
    Compute the DIV double differential cross section prediction.

    Parameters
    ----------
    dp: dps.darkphoton.DarkPhoton
        DarkPhoton instance for which the differential cross section is computed.
    scale: float, optional
        The PDF is evaluated at the energy scale: *scale dp.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0 (central value).
    eps: float, optional
        Numerical relative error. Default is 1e-3.

    Returns
    -------
    float: differential cross section
    """

    if dp.kT == 0:
        return np.nan

    Q2 = scale * dp.M**2
    if Q2 < _pdf.q2Min:
        log.warning("Interaction scale below the PDF lower bound for M={}.".format(dp.M))

    prefactor = _setup.P_beam / dp.Ek * 2 * _pdf.alpha.alphasQ2(Q2) \
              / (3 * np.pi * dp.kT**2 * _setup.S_beam)

    fxA = get_fxA(dp, Q2, mem)
    fxB = get_fxB(dp, Q2, mem)

    dcs_collinear = collinear_contribution(dp, fxA, fxB, Q2, mem, eps)
    dcs_soft = soft_contribution(dp, fxA, fxB)

    prefactor = _setup.P_beam / dp.Ek * 2 * _pdf.alpha.alphasQ2(Q2) \
              / (3 * np.pi * dp.kT**2 * _setup.S_beam)
    dcs = prefactor * (dcs_collinear + dcs_soft)

    return dcs

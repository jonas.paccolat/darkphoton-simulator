# -*- coding: utf-8 -*-

import numpy as np
from scipy.integrate import quad

from dps import setup
_setup = setup.Setup()

def get_kinematics(M, z, kT):
    """
    Compute the kinematic variables associated to the given dark photon momentum.
    """

    MT = np.sqrt(M**2 + kT**2)
    kL = z * _setup.P_beam
    Ek = np.sqrt(MT**2 + kL**2)
    yCM = _setup.y0 + 0.5*np.log((Ek+kL) / (Ek-kL))
    xA = M / np.sqrt(_setup.S_beam) * np.exp(yCM)
    xB = M / np.sqrt(_setup.S_beam) * np.exp(-yCM)

    return yCM, xA, xB, MT, kL, Ek

def get_indomain(M, kT, MT, xA, xB, yCM, eps=0):
    """
    Returns True if the dark photon momentum lies inside the allowed domain.

    The on-shell condition and the energy condition are checked.
    """

    energy = np.arccosh(np.sqrt(_setup.S_beam) / MT)
    onshell = xA * MT / M + kT**2 / _setup.S_beam / (1 -  xB * MT / M)

    cond1 = np.logical_and(-energy + eps <= yCM, yCM <= energy - eps)
    cond2 = np.logical_and(M**2 / _setup.S_beam + eps <= onshell, onshell <= 1 - eps)
    cond = np.logical_and(cond1, cond2)

    return cond

def plus_convolution(F, G, a, args, eps=1e-3):
    """
    Product convolution of the function G and the "+" distribution F.
    """

    assert 0 <= a < 1, 'the lower bound {} must be in the range [0,1).'.format(a)

    def G_local(x):
        return G(x, *args)

    G_local1 = G_local(1)

    def func(x):
        return F(x) * (G_local(x) - G_local1)

    I1 = quad(func, a, 1, epsrel=eps, epsabs=0, limit=1000)[0]
    I2 = quad(F, 0, a, epsrel=eps, epsabs=0, limit=1000)[0]

    return I1 - G_local1 * I2

def convolution(F, G, a, args, eps=1e-3):
    """
    Product convolution of the functions F and G.
    """

    assert 0 <= a < 1, 'the lower bound {} must be in the range [0,1).'.format(a)

    def func(x):
        return F(x) * G(x, *args)

    I1 = quad(func, a, 1, epsrel=eps, epsabs=0, limit=1000)[0]

    return I1

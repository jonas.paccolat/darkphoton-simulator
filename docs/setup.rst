=========
dps.setup
=========

This class defines the collision setup. All kinetic and energetic fixed
parameters are stored.

The global variables are set to the SHiP facility parameters by default. One
should modify them to consider a different experiment.

Class
-----

.. autoclass:: dps.setup.Setup
	:members:
	:private-members:

========
dps.data
========

Subpackage for handling the data.

.. toctree::
   :maxdepth: 1

   data/sort

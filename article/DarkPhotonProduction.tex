%
\documentclass[%
 reprint,
superscriptaddress,
%groupedaddress,
%unsortedaddress,
%runinaddress,
%frontmatterverbose, 
%preprint,
%showpacs,
preprintnumbers,
nofootinbib,
%nobibnotes,
%bibnotes,
 amsmath,amssymb, aps,
%pra,
%prb,
%rmp,
%prstab,
%prstper,
%floatfix,
]{revtex4-1}
%\pdfoutput=1
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{mathtools}

\usepackage{xcolor}


\bibliographystyle{unsrt}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}


\title{Dark photon production, NLO}% Force line breaks with \\


\author{J. Paccolat}
 \email{jonas.paccolat@epfl.ch}
\affiliation{Institute of Physics, Laboratory for Particle Physics and Cosmology,\\
\'{E}cole Polytechnique F\'{e}d\'{e}rale de Lausanne, CH-1015 Lausanne, 
Switzerland}%


\author{I. Timiryasov}
 \email{inar.timiryasov@epfl.ch}
\affiliation{Institute of Physics, Laboratory for Particle Physics and Cosmology,\\
\'{E}cole Polytechnique F\'{e}d\'{e}rale de Lausanne, CH-1015 Lausanne, 
Switzerland}


\begin{abstract}

The SHiP (Search for Hidden Particles) collaboration has proposed a multi-purpose experiment, which should gather data from SPS---400 GeV---protons during Run 3. This work extends some previous predictions \cite{SHiP} on the sensitivity of the detector to the production of dark photons. In particular it provides an efficient pythonic tool to compute the hadronic production at next-to-leading order using the CSS resummation formula \cite{Collins1985}. Besides the accuracy improvement from the previous simulations, our work reveals the so far neglected transverse momentum dependence. A discussion on the Bremstrahlung production channel is also presented. 

\end{abstract}

\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:intro}

In the very broad and debated quest for Dark Matter (DM), a new possibility of collider production (SHiP, NA64, FASER, SeaQuest,...) has emerged as very promising in the last few years. It is hypothesized that the yet non-observation of dark matter is due to its tiny interaction strength. This intensity frontier paradigm is to be opposed to the energy frontier paradigm which assumes very heavy dark matter particles. This last possibility has been intensely probed by large scale experiments (LHC or Tevatron) without success in the past decades. The new paradigm does not only rely on strong theoretical arguments but it is also experimentally attractive for its relatively low costs \cite{SHiP,US}. \\

The above cited collaborations aim at discovering some very weakly interacting long lived particles, such as Heavy Neutral Leptons as well as vector, scalar or axion portals to the Hidden Sector (HS) with small scale beam dump experiments. Our work is restricted to the production of vector portals---so-called dark photons and a particular attention is paid to the SHiP (Search for Hidden Particles) facility which characteristics are considered for the simulations. \\

The paper is organized as follows...


\subsection{Dark photons} % (fold)
\label{subsec:dark_photons}

One possible extension of the Standard Model (SM), which could be probed by the intensity frontier type experiments is the addition of a Hidden Sector (HS) completely neutral under the forces of the SM and of a new force which bridges both sectors. This work is restricted to vector portals even though models with scalar portals, neutrino portals or non-renormalizable portals also lead to promising results. The HS is assumed to possess a $U(1)_{HS}$ gauge group which is coupled to the SM through its kinetic term, namely
\begin{equation*}
    \mathcal{L}=\mathcal{L}_{SM}+\mathcal{L}_{DS}-\frac{\epsilon}{2}F_{\mu\nu}F'_{\mu\nu}\hspace{3pt},
\end{equation*}
where $F_{\mu\nu}$ and $F'_{\mu\nu}$ are respectively the field strength tensor of the SM and of the HS $U(1)$ groups. In view of the introduction, the HS quest relies on the assumption that the mixing angle is very small, $\epsilon \ll 1$, while the masses of the dark particles and their couplings remain sizable. We remain agnostic as to the matter composition of the HS Lagrangian and shall simply refer to the dark particles as $\chi$, so that
\begin{equation*}
    \mathcal{L}_{DS}=-\frac{1}{4}(F'_{\mu\nu})^2+\frac{1}{2}M^2(A'_{\mu})^2+\mathcal{L}_{\chi}\hspace{3pt}.
\end{equation*}
The signature of dark photons $A'$ are mainly sensitve to its mass $M$ and the coupling $\epsilon$. The decay rate can be computed for any point in the parameter space $(\epsilon, M)$ by considering lepton, hadron and dark particle decay products. In the most predictive scenario $M < 2 m_{\chi}$, where $m_{\chi}$ is the mass of the lightest dark particle charged under $U(1)_{DS}$, only SM decay channels matter. In section \ref{sec:ship_sensitivity} we shall give the sensitivity of the SHiP facility to such a scenario.\\ 


% subsection dark_photons (end)


\subsection{Direct production} % (fold)
\label{subsec:direct_production}

What is still debated and motivates this work is the computation of the different production channels. Using the equation of motion $\partial_{\mu}F_{\mu\nu}=e J_{\mu}^{EM}+o(\epsilon)$, the dark photon vertex takes the simple effective form 
\begin{equation}
     \epsilon e A'_{\mu}J_{\mu}^{EM}+o(\epsilon^2)\hspace{3pt},
\end{equation}
where $J_{\mu}^{EM}$ is the electromagnetic current. The dark photon may thus be treated as a massive photon of electromagnetic coupling $\epsilon e$. In the particular case of proton beam dump experiments, the dominant production processes, from higher to lower mass $M$, are the following \cite{SeaQuest} :
\begin{enumerate}
    \item Hard Drell-Yan collision : For masses above $\Lambda_{DY} \approx 1$ GeV the dominant production comes from parton interactions. 
    \item Proton Bremsstrahlung : The incident proton radiates a dark photon while it interacts almost elastically with the target.
    \item Meson decay :  Secondary mesons decay  into dark photons with the associated $\epsilon^2$ suppression factor.
\end{enumerate}
An accurate determination of the differential cross section of these different production channels is crucial for the design of the emulsion detector of the SHiP experiment. An estimation of the meson decay production channel is given in \cite{Pospelov}. The Bremstrahlung process is more delicate and we shall discuss the approach proposed by \cite{Blumlein} in section \ref{sec:a_comment_on_bremsstrahlung}. However, the core of this work is the improvement to next-to-leading order of the hard production channel, which reveals the transverse momentum dependence lacking at leading order. \\

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{Figures/setup.pdf} 
	\caption{SHiP experiment setup and the three dominant production channels depending on the dark photon mass. \textcolor{red}{Mensuration of the experiment ?}.}
	\label{fig:setup}
\end{figure}

% subsection direct_production (end)

\section{Hard production} % (fold)
\label{sec:hard_production}

In this section we shall compute and compare the naive parton model (NPM) single differential cross section \cite{FieldBook}, the next-to-leading order (NLO) integrated cross section \cite{Handbook} and the Collins-Soper-Sterman (CSS) double differential cross section \cite{Collins1985}. In subsection \ref{subsec:theory} the different formalisms are presented for the considered dark photon production and their implementation is discussed in section \ref{subsec:implementation}.

\subsection{Theory} % (fold)
\label{subsec:theory}

In the inclusive dark photon production the energy scale of the interaction is fixed by the mass $M$ of the vector portal. Hence, for masses above the QCD scale the dominant process is hard in the sense that the dark photon emerges from a direct collision of two parton cascades (short range), while the rest of the protons interact through soft processes (long range). The short range interaction has a perturbative expansion while the long range one is described by phenomenological quantities: the Parton Distribution Funtions (PDF). These describe the probability to find a given parton with a given momentum inside a hadron \cite{Collins2003}.\\

The general expression for the inclusive production of a dark photon $A'$ together with any hadronic state $X$ from two protons $A$ and $B$ is
\begin{multline*}
    d\sigma(AB\to A' X)=\\
    \sum_{a,b}\int_{x_A}^1 d\xi_A\int_{x_B}^1 d\xi_B f_{a/A}(\xi_A,\mu_F)f_{b/B}(\xi_B,\mu_F) d\hat{\sigma}(a b\to A')\hspace{3pt},
\end{multline*}
where the sum runs over any two partons $a$ and $b$ (quark, anti-quark or gluon) and the integral is over the longitudinal fractions of the hadronic momenta $P_{A/B}$ carried away by the partons of respective momenta $p_{a/b}=\xi_{A/B}P_{A/B}$. The very reason of the success of the parton description is the factorization property of the hadronic interaction exemplified in the above expression \citep{Collins1989}. The hadronic interaction is obtained as a weighted sum over all parton configurations of the associated partonic interaction $d\hat{\sigma}$. The weight corresponding to the configuration with parton momenta $p_a$ and $p_b$ is given by the product of PDFs $f_{a/A}(\xi_A,\mu_F)f_{b/B}(\xi_B,\mu_F)$. The factorization scale $\mu_F$ which separates the short range from the long range physics is unphysical. Nevertheless for troncated perturbative expansions the choice of the factorization scale matters \cite{Maltoni}.

\subsubsection{Leading order} % fold
\label{subsubsec:leading_order}

Let's define the parameters in the laboratory frame where the incoming and target proton momenta are
\begin{equation*}
    P_A=\big(E_A,0,0,P\big)\hspace{15pt}\mbox{and}\hspace{15pt}P_B=\big(m,0,0,0\big)\hspace{3pt},
\end{equation*}
with $E_A=\sqrt{m^2+P^2}$ and $m$ the proton mass. The momentum of the dark photon is characterized by the two variables $0<z<1$ and $k_T>0$, namely
\begin{equation}\label{eq:LABmomentum}
    k=\big(E_k,k_T,0,k_L\big)\hspace{3pt},
\end{equation}
with $E_k=\sqrt{M^2+k_T^2+k_L^2}$ and $k_L=z P$. Note that the $x$-axis is defined along the direction of the transverse momentum of the dark photon.\\

At leading order, the dark photon is created from the annihilation of a quark-antiquark pair. This simple vertex  is characterized by the Born amplitude \cite{EllisBook}
\begin{equation}
    \overline{\sum_{pol}}|\mathcal{M}^{LO}|^2=\frac{\sigma_0}{\pi}M^2\hspace{3pt}\mbox{, with}\hspace{3pt}\sigma_0=\frac{4\pi^2}{3}Q_q^2\epsilon^2\alpha\hspace{3pt},
\end{equation}
where $Q_q$ is the charge of quark $q$ in $e$-unit and $\alpha$ is the fine structure constant. In this limit the dark photon remains collinear to the proton beam with a momentum coresponding to the following differential cross section
\begin{equation}\label{eq:NPM}
    \frac{d\sigma^{NPM}}{dz}=\sum_{q}\frac{\sigma_0}{zs}f_{q/A}(x_A)f_{\bar{q}/B}(x_B)\hspace{3pt},
\end{equation}
where the sum runs over all quarks and antiquarks, $s$ is the center of mass energy and
\begin{equation*}
    x_A=\frac{M}{\sqrt{s}}e^y\hspace{15pt}\mbox{and}\hspace{15pt}x_B=\frac{M}{\sqrt{s}}e^{-y} \hspace{3pt},
\end{equation*}
are kinematic bounds. The rapidity in the CM frame is given by $y=\frac{1}{2}\log(x_A/x_B)$. 

%subsubsection leading_order (end)

\subsubsection{Next-to-leading order} % (fold)
\label{subsubsec:next_to_leading_order}

The computation of the NLO correction is important for two reasons: (1) it improves the accuracy---the K-factor is close to two, and (2) it reveals information on the transverse profile of the differential cross section.\\
At NLO, diagrams with one additional loop or one additional leg must be considered (see figure \ref{fig:diagrams}). All the loops are UV divergent and lead to the renormalization of the strong coupling $\alpha_s\to\alpha_s(\mu_R)$. Also and most importantly, all the diagrams are IR divergent. They exhibit soft divergences ($q\to0$) as well as collinear divergences ($q \parallel p_a$ or $q \parallel p_b$). Here $q$ is the momentum of the additional line. These long distance singularities are obviously not physical and merely reflect our poor understanding of the non-perturbative QCD effects. Nevertheless, the collinear divergences can be absorbed into scale dependent PDFs, which hide the actual non-perturbative behaviour behind phenomenological input \cite{EllisBook}. In the following the factorization scale and the renormalization scale are chosen identical $\mu_R=\mu_F$. \\

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{Figures/diagrams.png} 
	\caption{(a) NLO loop corrections. (b) NLO two-particle final states: Each diagram is accompanied by a cross channel.}
	\label{fig:diagrams}
\end{figure}

The way to deal with the remaining soft divergences depends on the scales at play. For the integrated cross section and the differential cross section at large $k_T > \Lambda_{QCD} \approx 1 \: GeV$ the soft divergences cancel each other at each perturbative order \cite{}. However in the small $k_T$ regime the divergences must be resummed (see section \ref{sec:}). \\

\paragraph{Integrated cross section}
At NLO the $\overline{\text{MS}}$ regularisation scheme leads to the following integrated cross section \cite{}

\begin{widetext}
\begin{align}\label{eq:intNLO}
\sigma_{NLO}(M,\mu^2) &= \sum_q \frac{\sigma_0}{s} \int_0^1 \frac{d\xi_A}{\xi_A}\:\frac{d\xi_B}{\xi_B} \:dx \: \delta(\frac{\tau}{\xi_A\xi_B}-x) \\
&\times \Bigg\{ \Big[ f_{q/A}(\xi_A,\mu^2)f_{\bar{q}/B}(\xi_B,\mu^2) + q \leftrightarrow \bar{q} \Big] \times \Big[ \delta(1-x)+\frac{\alpha_s}{\pi} \Big( \log \frac{M^2}{\mu^2} P_{qq}(x)+D_q(x) \Big)\Big] \\
&+ \Big[f_{g/A}(\xi_A,\mu^2)f_{q/B}(\xi_B,\mu^2) + f_{q/A}(\xi_A,\mu^2)f_{g/B}(\xi_B,\mu^2) + q \leftrightarrow \bar{q} \Big] \times \frac{\alpha_s}{\pi} \Big(\log \frac{M^2}{\mu^2} P_{qg}(x)+D_g(x)\Big) \Bigg\}
\end{align}
\end{widetext}

where $\mu$ is the factorization scale and the sum runs over all quarks. The splitting functions $P$ and the coefficient functions $D$ are given in \cite{Handbook}. These functions are composed of three terms. One proportional to a "+" distribution, one proportional to a $\delta$-distribution and one finite term. \\

\paragraph{Differential cross section}
In the large $k_T$ regime there is no need of soft divergences regulation and the differential cross section reads
\begin{equation}
\begin{split}
\frac{d\sigma}{dzdk_T^2} = \frac{P}{E_k} \frac{1}{16 \pi s^2} \sum_{ab} \int_0^1 \frac{d\xi_A}{\xi_A}\int_0^1 \frac{d\xi_B}{\xi_B} \delta(F(\xi_A, \xi_B)) \\ 
\times \: f_{a/A}(\xi_A)f_{b/B}(\xi_B) \hspace{2pt}\omega_{ab}(\xi_A,\xi_B) 
\end{split}
\end{equation}
with the NLO partonic amplitudes
\begin{align*}
\omega_{q\bar{q}} &= \frac{32 \sigma_0\alpha_s}{3}\big(\frac{2\hat{s}M^2}{\hat{u}\hat{t}}+\frac{\hat{t}}{\hat{u}}+\frac{\hat{u}}{\hat{t}}\big)\hspace{3pt}, \nonumber \\
\omega_{qg} &=-4\sigma_0\alpha_s\big(\frac{2\hat{u}M^2}{\hat{s}\hat{t}}+\frac{\hat{t}}{\hat{s}}+\frac{\hat{s}}{\hat{t}}\big)\hspace{3pt},\\
\omega_{gq} &=-4\sigma_0\alpha_s\big(\frac{2\hat{t}M^2}{\hat{s}\hat{u}}+\frac{\hat{u}}{\hat{s}}+\frac{\hat{s}}{\hat{u}}\big)\hspace{3pt}. \nonumber
\end{align*}
The prefactor $P/E_k$ arises from the transformation to the laboratory frame and the argument of the $\delta$-function ensuring the conservation of energy is defined in appendix equation \ref{eq:F}. The sum runs over all partons (quarks, antiquarks and gluons) but only the above $\omega$ combinations contribute at NLO. Changing the variables of integration to the partonic Mandelstam variables
\begin{equation*}
\hat{t} = M^2 (1-\frac{\xi_B}{x_B}\frac{M_T}{M}) \mbox{  and  } \hat{u} = M^2 (1-\frac{\xi_A}{x_A}\frac{M_T}{M})
\end{equation*}
where $M_T^2 = M^2 + k_T^2$ leads to 
\begin{equation}\label{eq:NLO}
\begin{split}
\frac{d\sigma}{dzdk_T^2} &= \frac{P}{E_k} \frac{1}{16 \pi s^2} \\
&\times \sum_{ab} \int_{\hat{t}_1}^{\hat{t}_2} \frac{d\hat{t}}{\hat{t}} \: \frac{f_{a/A}(\xi_A)f_{b/B}(\xi_B) \hspace{2pt}\omega_{ab}(\hat{t}, \hat{u}^*)}{M^2-\hat{t}}
\end{split}
\end{equation}
where $\hat{u}^* = (M^2-\hat{t}) \: k_T^2 / (k_T^2+\hat{t})$, the integral boundaries are obtained from the $\delta$-function and the partonic fractions are implicit functions of $\hat{t}$.\\

\paragraph{CSS resummation}
In the small $k_T$ regime, the differential cross section doesn't exhibit a cancellation of soft divergences. Non-negligible contributions from soft gluon lines occur at each order in pertubation theory. Collins, Soper and Sterman \cite{Collins1985} showed that all these divergences may be resummed into a Sudakov factor. Their CSS method applied to the dark photon production in the laboratory frame reads
\begin{equation}\label{eq:CSS}
    \frac{d\sigma^{CSS}}{dzdk_T^2}=\frac{1}{2z}\int_0^{\infty}db\: b\: J_0(k_T b)\:\mathcal{W}(b,M,x_A,x_B)\hspace{3pt},
\end{equation}
where $J_0$ is the Bessel function of the first kind and 
\begin{equation}\label{eq:W}
   \mathcal{W}(b,M,x_A,x_B)=\text{e}^{-S(b,M)}W(b,x_A,x_B)\hspace{3pt},
\end{equation}
with
\begin{align*}
    S(b,M)&=\int_{b_0^2/b^2}^{M^2} \frac{d\bar{\mu}^2}{\bar{\mu}^2} \Big[\mathcal{A}\big(\alpha_s(\bar{\mu})\big)\log \frac{M^2}{\bar{\mu}^2}+\mathcal{B}\big(\alpha_s(\bar{\mu})\big)\Big]\hspace{3pt},\\
    W(b,x_A,x_B)&=\sum_q \frac{\sigma_0}{s} \mathcal{P}_{q/A}(x_A,b) \mathcal{P}_{\bar{q}/B}(x_B,b)\hspace{3pt}.
\end{align*}
The distributions $\mathcal{P}_{q/A}(x_A,b)$ and $\mathcal{P}_{\bar{q}/B}(x_B,b)$ describe the "probability" that the $q\bar{q}$ pair annihilates with the associated rapidity and a transverse momentum up to $b_0/b$, with $b_0 = 2e^{-\gamma_E}\approx1.12$. Hence the sum is only over quark and antiquark flavors. However, the presence of the quark $q$ in the hard interaction may occur from a series of parton branching triggered by any possible parton $a$ (or $b$) in the parent parton $A$ (or $B$), namely
\begin{equation*}
    \mathcal{P}_{q/A}(x,b)=\sum_a \int_x^1 \frac{d\xi}{\xi} f_{a/A}(x,b_0/b) \mathcal{C}_{qa}\big(\frac{x}{\xi},\alpha_s(b_0/b)\big)\hspace{3pt}.
\end{equation*}
The coefficient functions $\mathcal{C}_{ab}$ are process dependent, while the usual PDFs account for the collinear divergences. Note that the energy scale at which the different quantities are computed varies with the impact parameter $b$, so that a perturbative expansion is only reliable in the range $b \lesssim 1/\Lambda_{QCD}$. In that region one can expand the above resummed cross section to any fixed order and match it with the result obtained in perturbation theory. A detailed computation of $\mathcal{A}$, $\mathcal{B}$ and $\mathcal{C}$ up to NLO can be found in \cite{PerfectDY}. However, in the region $b\gtrsim 1/\Lambda_{QCD}$, a perturbative treatment alone is not sufficient and one must introduce a non-perturbative parametrization \footnote{In the large $M$ limit, the integrand of the differential cross section \eqref{eq:CSS} is dominated by a saddle point in the perturbative region, so that the non-perturbative input can safely be neglected \cite{Collins1985}. The larger the mass the better the saddle point approximation and the smaller its abscissa. Unfortunately, the considered mass scale is too small to justify such an approximation.}. Different phenomenological models have been suggested in the literature \cite{Landry2003}. Here we shall follow \cite{Konychev2006} who gave an improvement of the original idea of \cite{Landry2003}. The idea is to keep the perturbative expressions, but to replace the energy at which they are evaluated by the bounded function
\begin{equation*}
  b^*=\frac{b}{\sqrt{1+b^2/b_{max}^2}}\hspace{3pt},  
\end{equation*}
where the parameter $b_{max}$ separates the perturbative regime from the non-perturbative regime. If we call $\mathcal{W}_{pert}$ the quantity \eqref{eq:W} in the perturbative region, then we may update it over the whole $b$-range to 
\begin{equation*}
    \mathcal{W}(b)=\mathcal{W}_{pert}(b^*)e^{-F_{NP}(b)}\hspace{3pt},
\end{equation*}
where the non-perturbative correction is
\begin{equation*}
    F_{NP}(b)=\big[a_1+a_2 \log \frac{M}{M_0}+a_3\log (100 x_A x_B)\big] b^2\hspace{3pt}.
\end{equation*}
In the small $b$ limit, the perturbative prediction is recovered since $b^*\to b$ and $F_{NP}\to 0$. According to the stability study of \cite{Konychev2006}, the following parameters are used in the simulations: $b_{max}=1.5$ GeV$^{-1}$, $a_1=0.201$ GeV$^2$, $a_2=0.184$ GeV$^2$, $a_3=-0.026$ GeV$^2$ and $M_0=3.2$ GeV. One last comment is needed: according to the definition of $b^*$, the range of energies at which the PDFs will be evaluated along the $b$ integral is $[b_0/b_{max},\infty[$. However, the lower bound of this interval may be smaller than the lower bound $M_{min}$ of the set of PDFs that is used for the numerical computation. In that case, one should use $b_{max}=b_0/M_{min}$ instead of $b_{max}=1.5$ GeV$^{-1}$ for the PDFs energy scale, while keeping everything else unchanged.

% subsubsection next_to_leading_order (end)

\subsubsection{Matching small and large $k_T$ regimes} % (fold)
\label{subsubsec:matching_small_and_large_kT_regimes}

In order to interpolate between the CSS prediction \ref{eq:CSS} at small $k_T$ and the NLO prediction \ref{eq:NLO} at large $k_T$ a matching procedure must be introduced. The NLO differential cross section can be written as $d\sigma_{NLO} = d\sigma_{DIV} + d\sigma_{FIN}$, where terms proportional to $(1 + \text{ logs of } k_T) / k_T^2$ or $\delta(k_T)$ are gathered in the divergent part $d\sigma_{DIV}$ and the remaining terms constitute the finite part $d\sigma_{FIN}$. The expression of $d\sigma_{DIV}$ is explicited in \cite{} for a similar process. \\

The CSS resummation regulates the NLO differential cross section by replacing $d\sigma_{div} \xrightarrow{reg} d\sigma_{CSS}$. The NLO accurarcy is reached for a truncation of the CSS integral at NLL (next-to-leading logarithm). Hence at NLO, the intuitive procedure described in \cite{Collins1985} is simply to replace $d\sigma_{DIV}$ by $d\sigma_{CSS}$ at NLL, leading to
\begin{equation}
d\sigma = d\sigma_{CSS} + d\sigma_{NLO} - d\sigma_{DIV}
\end{equation} 
This definition explicitly implies the correct behaviour in the small $k_T$ regime. At large $k_T$ however, the correct behaviour can only be achieved if the CSS resummation doesn't alter the differential cross section in this regime. As pointed out in \cite{Boglione2014},  the independence depends on the mass of the outgoing particle. In particular for the considered range of dark photon masses, the CSS parametrization choice will influence the behaviour in the large $k_T$ regime. As a consequence the convergence $d\sigma_{CSS} \xrightarrow{k_T \gg \Lambda_{QCD}} d\sigma_{DIV}$ is only approximate, as shown on figure \ref{fig:matching}, and the NLO predicition is not recovered at large $k_T$.\\
\begin{figure}
\includegraphics[width=0.4\textwidth]{Figures/matching.png}
\label{fig:matching}
\caption{Differential cross section predicted by the three differnent expressions: NLO, DIV and CSS. The success / failure of the convergence $d\sigma_{NLO} \xrightarrow{k_T \ll \Lambda_{QCD}} d\sigma_{DIV}$ / $d\sigma_{CSS} \xrightarrow{k_T \gg \Lambda_{QCD}} d\sigma_{DIV}$ are illustrated. The transition momenta $k_{T1}$ and $k_{T2}$ are also depicted.}
\end{figure}

To remedy this issue we instead implemented the following inferior and superior bounds on the differential cross section
\begin{align}
d\sigma_{inf} &= 
\begin{cases}
d\sigma_{CSS} & \text{, if  } k_T < k_{T1} \\
d\sigma_{NLO} & \text{, if  } k_T > k_{T1}
\end{cases} \\
d\sigma_{sup} &= 
\begin{cases}
d\sigma_{CSS} & \text{, if  } k_T < k_{T2} \\
d\sigma_{NLO} & \text{, if  } k_T > k_{T2}
\end{cases}
\end{align}
with the transition momenta $k_{T1}$ and $k_{T2}$ respectively defined as the momenta of first and second intersection between the NLO and the CSS prediciton, as pictured on figure \ref{fig:matching}.


% subsubsection matching_small_and_large_kT_regimes (end)

% subsection theory (end)

\subsection{Implementation}
\label{subsec:implementation}

In this section we discuss the implementation of the $CSS$, $DIV$ and $NLO$ predictions for the double differential cross section $d\sigma/dzdk_T^2(M)$, the $NPM$ prediction for the single differential cross section $d\sigma/dz(M)$ as well as the $NPM$ and $NLO$ predictions for the integrated cross section $\sigma(M)$.

\subsubsection{Fitting procedure}
\label{subsubsec:fitting_procedure}

The computational cost of the $CSS$ integral has motivated the following fitting procedure:
\begin{enumerate}
\item Generate fixed mass dark photons over a grid covering the physical domain and compute their $NLO$ and $CSS$ differential cross section for a range of masses $M_i$. The boundaries of the physical domain are discussed in appendix \ref{ap:parton_dynamics}. In order to concentrate on the large differential cross section regions, the grid spacing follows the shifted Gaussian distribution presented in appendix \ref{ap:sample_distribution}.
\item Interpolate the data to the mass of interest $M$:
\begin{itemize}
\item Select the three closest masses $M_i$ encapsulating $M$.
\item Fit the $NLO$ and $CSS$ predictions for each of them. This step leads to boundary instabilities which are removed by a slight reduction of the physical domain.
\item Generate $(z, k_T)$ points covering the physical domain of a dark photon of mass $M$ according the the shifted Gaussian distribution. 
\item Compute the fitted $NLO$ and $CSS$ predictions for the three selected masses at each $(z, k_T)$ point and interpolate them to the mass of interest $M$.
\item Fit the interpolated data.
\item Use the fits to find the transition curves defining the superior and inferior bounds on the differential cross section as discussed in section \ref{subsubsec:matching_small_and_large_kT_regimes}.
\end{itemize}
\end{enumerate}
A python package was developed for a systematic implementation of the above procedure. It is configured for the SHiP facility setup for which it provides the step (1) data, but it could easily be used for other experiments. It can be downloaded from the GitLab repository \texttt{https://gitlab.com/ship-darkphoton} with its full documentation.

% subsubsection fitting_procedure (end)

\subsubsection{Cross sections}
\label{subsubsec:cross_sections}

Change sup, inf labels in figure \ref{fig:ds_dzdkT}.\\

The transverse profile of the double differential cross section is represented on figure \ref{fig:ds_dzdkT} for different masses and longitudinal fractions. The uncertainty conserning the matching procedure is illustrated by the discrepancy between the $INF$ and $SUP$ predictions. The abrupt cutoff at large $k_T$ occurs at the boundary of the squeezed phyisical domain.
\begin{figure*} 
\includegraphics[width=0.7\textwidth]{Figures/dcs.png}
\label{fig:ds_dzdkT}
\caption{Double differential cross section. Superior (full) and inferior (dashed) predictions of the transverse profile are illustrated for a set of masses and longitudinal fractions.}
\end{figure*}\\
Next, the $NPM$ prediction (eq. \ref{eq:NPM}) is compared to the transverse integration of the $INF$ and $SUP$ predictions on figure \ref{fig:ds_dz}, illustrating that it falls within the matching procedure uncertainty range. Note the reduction of the peak at small $z$ when the mass is increased.
\begin{figure}[h!]
\includegraphics[width=0.4\textwidth]{Figures/ds_dz.png}
\label{fig:ds_dz}
\caption{Single differential cross section. The NPM prediction (full line) lies between the $INF$ and $SUP$ predictions (shade). These are obtained by numerically integrating the differential cross section along $k_T$.}
\end{figure}\\
Finally figure \ref{fig:sigma} displays the integrated cross section predicted by the $NPM$ and the $NLO$ formulas together with their scale uncertaintes (shaded regions) and compare it to the numerical integration of the $INF$ and $SUP$ predictions for a set of masses. It appears that the $NLO$ prediction is close to the $SUP$ bound, suggesting that the correct matching procedure might be close to the $SUP$ procedure. At small masses however the $SUP$ bound doesn't reach the $NLO$ prediction, but as already stated the perturbative expension is poorly justified in this region and soft corrections should be considered.
\begin{figure}
\includegraphics[width=0.4\textwidth]{Figures/sigma.png}
\label{fig:sigma}
\caption{Integrated cross section. The NPM and NLO predictions (full lines) together with their scale and PDF uncertainties (shades) are compared to the $INF$ and $SUP$ predictions (obtained by numerical integration).}
\end{figure}

% subsubsection cross_sections (end)

\subsubsection{Uncertainties and approximations}
\label{subsubsec:uncertainties_and_approximations}

The computation of the differential and integrated cross section come with different sources of errors or uncertainties:

\paragraph{PDF uncertainties}
We make use of the LHAPDF6 library \cite{LHAPDF6} to access the PDF sets provided by the major fitting groups. The extraction of PDFs is subject to systematic and experimental errors. The propagation of these errors to any PDF dependent quantity can be obtain automatically with the LHAPDF6 tool. In particular we used the CT14nlo PDF set \cite{CT14} constituted of 56 set members, beside the central member, necessary for the uncertainty computation. The hessian error type was used with a 95\% confidence level. With these considerations the relative error $(d\sigma_+ - d\sigma_-) / (d\sigma_+ + d\sigma_-)$ of the double differential cross section varies between $0.15$ and $0.4$ with a tendency to increase at larger mass.

\paragraph{Scale uncertainties}
In this work the factorization and renormalization scales were both set to the dark photon mass, $\mu_R = \mu_F = M$. This conventional choice has no physical justification since the factorization scale is not physical, it only arise because of the perturbative truncation \cite{}. The scale uncertainty were thus quantified by measuring the variability of the considered quantities when varying the factorization scale from $M/2$ to $2M$, which lead to relative error variying between $0.3$ and $0.4$. A larger error is measured at $M=1$ GeV where the perturbative treatment is illegitimate (especially for $\mu=M/2$). Note that the CSS computation is independent of the choice of a scale since all scales are explicitly covered by the $b$-integral.

\paragraph{Parametrization scheme uncertainties}
Different propositions for the non-perturbative corrections of the CSS prediction have been proposed in the literature. In this work, the parametrization of \cite{Konychev2006} was used. A comparison with the parametrization \cite{} over a sample of 1000 $(z, k_T)$ points lead to a mean difference $\Delta_{mean} < 10\%$ and a maximal difference $\Delta_{max} < 30\%$ for masses above 2 GeV. At $M=1$ GeV the discrepancy was much larger with $\Delta_{mean} \approx 48\%$ and $\Delta_{max} \approx 68\%$, which merely reflects the limit of the perturbative QCD regime. 

\paragraph{Numerical errors}
Numerical errors arise from the multiple integrals encountered and are fixed below the per mille accuracy. Only the CSS expression requires some further considerations. First, the integral \ref{eq:CSS} is truncated at $b_{max}=10$ GeV $^{-2}$ which ensures the same precision. Second, because of the combination of different integrals the error must be propagated as discussed in appendix \ref{ap:numerical_errors}. 

\paragraph{Simplificative assumptions}
The perturbative expansion is truncated at next-to-leading order. Higher order perturbative computation for the Drell-Yan process suggest that this omission is leads to negligible errors \cite{Hamberg_1990}. Also the partons are assumed massless and consequently only the PDFs associated to the three lighter quarks are considered.

\paragraph{Domain squeezing}
As a consequence of the fitting procedure, unphysical oscillations may arise close to the boundary of the physical domain. These were removed with a slight reduction of the physical domain. The loss of information was estimated by the following ratio:
\begin{equation*}
\int_{SD} dz dk_T \: \frac{d\sigma}{dz dk_T}(z, k_T) \: / \: \int_{ND} dz dk_T \: \frac{d\sigma}{dz dk_T}(z, k_T^*(z))
\end{equation*} 
where $SD$ and $ND$ respectively stand for Squeezed Domain and Neglected Domain and $k_T^*(z)$ is the boundary of $SD$. This quantiy remained below $10^{-3}$ over the whole considered mass range.

% subsubsection uncertainties_and_approximations (end)


\subsubsection{Time consumption}
\label{subsubsec:time_consumption}

The implementation of the fitting procedure allows an improvement of the time needed to compute the differnetial cross section by five orders of magnitude. Because of the vectorized construction it is most efficient when computing at least $10^6$ points at a time. 

% subsubsection time_consumption (end)


% section hard_production (end)


\section{SHiP sensitivity} % (fold)
\label{sec:ship_sensitivity}

% section ship_sensitivity (end)

\section{A comment on bremsstrahlung} % (fold)
\label{sec:a_comment_on_bremsstrahlung}





% section a_comment_on_bremsstrahlung (end)

\section{Conclusions} % (fold)
\label{sec:conclusions}

% section conclusions (end)

%%%%%%%%%%%%% APPENDIX %%%%%%%%%%%%

\section{Appendix}
\label{sec:appendix}

\subsection{Parton dynamics} % (fold)
\label{ap:parton_dynamics}

A thorougher investigation of the parton phase space leads to constraints on the dark photon momenta. The partonic dark photon production is described by four dynamical variables: $\xi_A$, $\xi_B$, $k_T$ and $y$ (or equivalently $z$). Because a parton momentum can't exceed its parent proton momentum and because the parton center of mass energy must allow the dark photon production, the parton longitudinal fractions respect $M^2/s < \xi_A,\xi_B <1$. Also, the on-shell condition of the outgoing parton of mass $\mu$ leads to the constraint \cite{PerfectDY}
\begin{equation}\label{eq:F}
F(\xi_A, \xi_B) = (\xi_A-x_A\frac{M_T}{M})(\xi_B-x_B\frac{M_T}{M}) - \frac{\mu^2+k_T^2}{M^2}
\end{equation}
with the transverse mass $M_T = \sqrt{M^2+k_T^2}$. The combination of this expression and the domain of the longitudinal fraction translates into
\begin{equation}\label{eq:physical_domain}
\frac{M^2}{s} < x_A \frac{M_T}{M}+\frac{\mu^2+k_T^2}{M^2}\frac{1}{1-x_B M_T/M} < 1
\end{equation}
which sets a boundary in the $(z,k_T)$-plane of the dark photon. (\textcolor{red}{figure ???}). \\

A few comments are required here. First, the on-shell condition results from the pQCD negligence of power suppressed off-shell lines. Second, the resultant parton mass and energy are positive because the outgoing parton system is the source of hadronic jets. The positive energy condition wasn't discussed because much weaker. Third, at next-to-leading order there is only one outgoing parton, which is assumed massless, so that $\mu = 0$. At higher order, resultant outgoing parton with $\mu > 0$ correspond to stronger constraints on the $(z,k_T)$-plane. Third, at leading order, no outgoing parton is emitted holding the dark photon on the beam axis with momentum in the corresponding above range.


% subsection parton_dynamics (end)

\subsection{Sample distribution} % (fold)
\label{ap:sample_distribution}

As mentioned in section \ref{} in order to best account for the fast amplitude change of the cross section, the sample of fitted dark photons where generated according to a shifted Gaussian distribution along both longitudinal and transverse momenta. This procedure is made explicit in this section.\\

Let's consider the interval $[x_{min},x_{max}]$ which must be split in $N$ steps according to the distribution
\begin{equation*}
	f(x) = A(e^{-(x-x_0)^2/2\sigma^2}+a)
\end{equation*}
where the mean $x_0 \in [x_{min},x_{max}]$ and the variance $\sigma^2$ are fixed a priori, while the shift $a \in \mathbb{R}$ and the normalization $A \in \mathbb{R}$ are adjusted a posteriori to guarantee that the size of the last step equals $dx\equiv dx_N$ and that the number of steps is $N$. We look for the coordinates $x_i$ and their associated steps $dx_i=1/f(x_i)$ for all indices $i=0,1,\dots,N$. Defining the continuous index $\alpha \in [0,N]$, the following integral equation
\begin{equation*}
	\alpha(x) = \int_{x_{min}}^x f(x') dx' = \text{func}(a,A)
\end{equation*} 
relates indices to their coordinates, i.e. if $\alpha=i \in \{0,1,\dots,N\}$ then the integral upper bound $x$ is the coordinate of the $i$st step. The parameters $a$ and $A$ are fixed by the conditions $\alpha(x_{max}) = N$ and $\alpha(x_{max}-dx) = N-1$, leading to 
\begin{align*}
a &= \frac{a^*\Delta x}{\Delta x-N dx} \Big[1-N\frac{erf(\frac{x_{max}-x_0}{\sqrt{2\sigma^2}})-erf(\frac{x_{max}-dx-x_0}{\sqrt{2\sigma^2}})}{E}\Big]\\
A &= \frac{N}{\Delta x (a-a^*)}
\end{align*}
with $\Delta x = x_{max}-x_{min}$, $a*=-\sigma \sqrt{\frac{\pi}{2}}E/\Delta x$ and $E=erf(\frac{x_{max}-x_0}{\sqrt{2\sigma^2}})+erf(\frac{x_0-x_{min}}{\sqrt{2\sigma^2}})$.\\

All sets of parameters won't lead to a meaningful distribution. Two conditions must be met:
\begin{enumerate}
	\item $A>0$, or equivalently $a>a^*$, which is satisfied for all $dx \in (\Delta x/N, \Delta x)$. 
	\item $f(x_{min})\geq 0$ and $f(x_{max})\geq 0$, which is satisfied for $a \geq a_{min}=-e^{-\delta z^2/2\sigma^2}$, with $\delta z = \text{max}(z_0-z_{min}, z_{max}-z_0)$. The associated $dx_{max}$ is found by solving $a(dx_{max}) = a_{min}$.
\end{enumerate}
Hence, a consistent distribution is obtained for $dx \in (\Delta x/N, \text{min}(\Delta x, dx_{max}))$, as illustrated in figure \ref{fig:distribution}.

\begin{figure}
\includegraphics[width=0.4\textwidth]{Figures/distribution.pdf}
\label{fig:distribution}
\caption{Above: Shift and normalization of the distribution function depending on the choice of the last step size $dx$. The other parameters are $x_{min}=0$, $x_{max}=10$, $x_0=2$, $\sigma=4$ and $N=100$. Only the region where the blue curve is larger than the dashed line corresponds to admissible values of $dx$. Below: Representation of the distributed $N$ coordinates for three different values of $dx$.}
\end{figure}

% subsection sample_distribution (end)

\subsection{Numerical errors} % (fold)
\label{ap:numerical_errors}

\begin{figure}
\includegraphics[width=0.4\textwidth]{Figures/errors.png}
\label{fig:errors}
\caption{}
\end{figure}

In the $CSS$ computation one must consider numerical errors $\epsilon_b$, $\epsilon_S$ and $\epsilon_P$ arising respectivlely from the $b$-integral (eq. \ref{eq:CSS}), the Sudakov integral (eq. \ref{}) and the parton distribution integral (eq. \ref{}). Propagating them through the computation leads to the $CSS$ relative error
\begin{equation}
\epsilon_{CSS} = \epsilon_b + \int db \: b \: J_0 \:(\epsilon_P +\epsilon_S S) \: \mathcal{W} \: / \: \int db \: b \: \mathcal{W}
\end{equation}
Over the considered mass range the Sudakov integral remains below 20, which motivates the choice $\epsilon_b = 4\epsilon_P = 40 \epsilon_S = 10^{-3}$ consistent with $\epsilon_{CSS} < 2\epsilon_b$.  Finally, the $b$-integral is cut at $b=20$ GeV$^{-2}$ which ensures the same accuracy.

% subsection numerical_errors (end)

% section appendix (end)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newpage

\bibliography{references}

\end{document}

# -*- coding: utf-8 -*-

import numpy as np
from scipy.interpolate import griddata, interp1d

from dps.data import sort
from dps.logs import log
from dps import sample

def check_data_structure(dataset):
    """Check the data structure compatibility with the module."""

    args, res = dataset
    assert len(args) == len(res) > 0
    assert {"z", "kT", "css", "div", "nlo"} == res[0].keys()

    return True

def fit(z, kT, dcs):
    """
    Fit the differential cross section *dcs* over the (*z*, *kT*)-plane.

    Parameters
    ----------
    z: (N) array-like
        List of the dark photon longitudinal fractions.
    kT: (N) array-like
        List of the dark photon transverse momenta.
    dcs: (N) array-like
        List of the differential cross sections.

    Returns
    -------
    Function interpolating the provided differential cross section data.
    """

    def f(z_, kT_):
        dcs_ = griddata((z, kT), dcs, (z_, kT_),
                         method='cubic', fill_value=0.)
        return dcs_

    return f

def set_grid(dp, sample_size):
    """
    Cover the physical domain of the dark photon *dp* ; inplace.

    A gaussian grid with *Nz* longitudinal fractions and *NkT* transverse
    momenta is defined to cover the physical domain of the dark photon *dp*. The
    gaussian ditribution is decribed in the module *dps.distribution*.

    The momenta are stored as attributes of *dp*.

    Parameters
    ----------
    dp: dps.darkphoton.DarkPhoton
        Dark photon with the considered mass.
    sample_size: (int, int)
        Number of longitudinal fractions (*Nz*) and number of transverse momenta
        (*NkT*).
    """

    Nz, NkT = sample_size
    z, kT = sample.sample(dp, "regular", (Nz, NkT), mesh="gaussian", args=None)

def get_interpolation_masses_and_data(dp, dataset):
    """
    Select the three nearest masses encapsulating the considered mass together
    with their associated data.

    Parameters
    ---------
    dp: dps.darkphoarkPhoton
        Dark photon with the considered mass.
    dataset: list
        List of the data dictionnaries.

    Returns
    -------
    interp_masses: list
        List of three increasing masses encapsulating the dark photon mass.
    interp_data: list
        List of three data dictionnaries associated to the three masses.
    """

    args, res = dataset

    # for r in res:
    #     sort.remove_singularity(r)

    # get increasing list of dark photon masses
    all_masses = [a.M for a in args]
    n = len(all_masses)

    # assert that the interpolated mass belongs to the computed mass range
    Mmin = all_masses[0]
    Mmax = all_masses[-1]
    if dp.M < Mmin or dp.M > Mmax:
        text = 'Provided mass (M={} GeV) out of the considered mass range '\
               '(M={} GeV to M={} GeV).'
        log.error(text.format(dp.M, Mmin, Mmax))

    # find the closest computed mass
    imin = np.argmin([np.abs(dp.M - M_) for M_ in all_masses])

    # set the interpolation masses
    if imin == n - 1:
        indices = [imin - 2, imin - 1, imin]
    elif imin == 0:
        indices = [imin, imin + 1, imin + 2]
    else:
        indices = [imin - 1, imin, imin + 1]

    # store interpolation masses and differential cross sections
    interp_masses = [all_masses[i] for i in indices]
    interp_data = [res[i] for i in indices]

    return (interp_masses, interp_data)

def interpolate_method(dp, key, interp):
    """
    Interpolate the differential cross section from the prediction *key* and
    evaluated it at the grid coordinates (darkp photon momenta).

    Parameters
    ----------
    dp: dps.darkphoarkPhoton
        Dark photon with the considered mass.
    key: str
        Prediction method: "css", "nlo" or "div".
    interp: (list, list)
        List of the three encapsulating masses and list of their data
        dictionnaries.

    Returns
    -------
    array: List of the differential cross section at the dark photon mass and
    momenta.
    """

    grid_datas = list()
    for M, data in zip(interp[0], interp[1]):
        f = fit(data["z"], data["kT"], data[key])
        grid_datas.append(f(dp.z, dp.kT))

    grid_datas = np.array(grid_datas)

    return interp1d(interp[0], grid_datas, kind=2, axis=0)(dp.M)

def interpolate(dp, sample_size, dataset):
    """
    Compute the interpolated differential cross section function at the
    considered dark photon mass (*dp.M*) for the three predictions: "css", "nlo"
    and "div".

    Parameters
    ----------
    dp: dps.darkphoton.DarkPhoton
        Dark photon with the considered mass.
    sample_size: (int, int)
        Number of longitudinal fractions (*Nz*) and number of transverse momenta
        (*NkT*).
    dataset: list
        List of the data dictionnaries.

    Returns
    -------
    dict: Dictionnary of the interpolated cross section with keys: "css", "nlo"
    and "div".
    """

    assert check_data_structure(dataset)

    # set momenta over a regular grid covering the dark photon physical domain
    set_grid(dp, sample_size)
    interp = get_interpolation_masses_and_data(dp, dataset)

    fits = dict()
    keys = ["css", "nlo", "div"]
    for key in keys:
        dcs = interpolate_method(dp, key, interp)
        fits[key] = fit(dp.z, dp.kT, dcs)

    return fits

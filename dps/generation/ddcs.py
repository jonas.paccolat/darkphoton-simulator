# -*- coding: utf-8 -*-

import traceback
import numpy as np

from dps import sample, darkphoton
from dps.data import sort
from dps.generation import div, nlo, css
from dps.logs import log

def generate_momenta(args):
    """
    Generate momenta covering the physical domain of the given mass with the
    given mesh.
    """

    dp = darkphoton.DarkPhoton(args.M)

    if args.mesh == "random":
        sample.sample(dp, "random", args.N, seed=args.seed,
               set_coord=True, quiet=True)
    elif args.mesh == "constant":
        sample.sample(dp, "regular", (args.Nz, args.NkT), mesh="constant",
               set_coord=True, quiet=True)
    elif args.mesh == "gaussian":
        sample.sample(dp, "regular", (args.Nz, args.NkT), mesh="gaussian",
               set_coord=True, quiet=True)
    else:
        log.error("Invalid mesh={}. Try 'random', 'constant' or 'gaussian'.".format(args.mesh))

    return dp

def central(args, scale=1., mem=0, methods={"css", "nlo", "div"}):
    """
    Compute the central differential cross section.

    Parameters
    ----------
    args: parser
        Namespace with the command line arguments. It fixes the dark photon mass
        and the covering of the physical domain.
    scale: float, optional
        The *NLO* and *DIV* differential cross section are computed at the scale
        *scale args.M*. Default is 1.
    mem: int, optional
        PDF member ID. Default is 0.
    methods: dict, optional
        Dictionnary of the prediction to consider. Default is *{css, nlo, div}*.

    Returns
    -------
    res: dict
        "z": array of the longitudinal fractions.
        "kT": array of the transverse momenta.
        "css": CSS differential cross section prediction.
        "div": DIV differential cross section prediction.
        "nlo": NLO differential cross section prediction.
    """

    dp = generate_momenta(args)

    n = len(dp.z)

    res = dict()
    res["z"] = dp.z
    res["kT"] = dp.kT
    res["css"] = np.zeros(n)
    res["nlo"] = np.zeros(n)
    res["div"] = np.zeros(n)

    for i in range(n):

        log.info("{}  %".format(100 * i // n))

        _dp = dp.select(i)

        if "css" in methods:
            res["css"][i] = css.differential_cross_section(_dp,
                                                           parametrization="BLNY",
                                                           mem=mem)
        if "nlo" in methods:
            res["nlo"][i] = nlo.differential_cross_section(_dp, scale=scale,
                                                           mem=mem)
        if "div" in methods:
            res["div"][i] = div.differential_cross_section(_dp, scale=scale,
                                                           mem=mem)

    try:
        res = sort.remove_singularity(res)
        args.clean = True
    except:
        log.warning("Couldn't clean data file {}. Error message:".format(args.pickle))
        traceback.print_exc()
        args.clean = False

    return res, args

def uncertainty(args, cl=95, Nscale=9, Dscale=2.):
    """
    Compute the PDF and scale uncertainties of the differential cross section.

    Parameters
    ----------
    args: parser
        Namespace with the command line arguments. It fixes the dark photon mass
        and the covering of the physical domain.
    cl: int, optional
        Confidence level (in percent) for the LHAPDF uncertainty
        computation. Default is 95.
    Nscale: int, optional
        Number of gradation in the scale interval. Default is 9.
    Dscale: float, optional
        The scales are varied between *args.M / Dscale* and *Dscale args.M*.
        Default is 2.

    Returns
    -------
    res: dict
        "z": array of the longitudinal fractions.
        "kT": array of the transverse momenta.
        "css": array of the CSS differential cross section prediction. ("central" and "pdf")
        "div": array of the DIV differential cross section prediction. ("central", "pdf" and "scale")
        "nlo": array of the NLO differential cross section prediction. ("central", "pdf" and "scale")
    """

    _pdf = css.get_pdf()

    dp = generate_momenta(args)

    n = len(dp.z)

    res = dict()
    res["z"] = dp.z
    res["kT"] = dp.kT
    res["css"] = {"central": list(), "pdf": list()}
    res["div"] = {"central": list(), "pdf": list(), "scale": list()}
    res["nlo"] = {"central": list(), "pdf": list(), "scale": list()}

    for i in range(n):

        log.info("{} %".format(100 * i // n))

        _dp = dp.select(i)

        delta_pdf = {"css": list(), "nlo": list(), "div": list()}
        for mem in range(_pdf.pset.size):
            delta_pdf["css"].append(css.differential_cross_section(_dp, parametrization="BLNY", mem=mem))
            delta_pdf["nlo"].append(nlo.differential_cross_section(_dp, scale=1, mem=mem))
            delta_pdf["div"].append(div.differential_cross_section(_dp, scale=1, mem=mem))

        for method in ["css", "nlo", "div"]:
            hessian = _pdf.pset.uncertainty(delta_pdf[method], cl)
            res[method]["central"].append(hessian.central)
            res[method]["pdf"].append((hessian.central - hessian.errminus,
                                       hessian.central + hessian.errplus))

        delta_nlo = [nlo.differential_cross_section(_dp, scale=Dscale**j) \
                              for j in np.linspace(-1, 1, Nscale)]
        delta_div = [div.differential_cross_section(_dp, scale=Dscale**j) \
                              for j in np.linspace(-1, 1, Nscale)]
        res["nlo"]["scale"].append((min(delta_nlo), max(delta_nlo)))
        res["div"]["scale"].append((min(delta_div), max(delta_div)))

    return res, args

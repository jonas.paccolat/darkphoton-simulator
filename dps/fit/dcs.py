# -*- coding: utf-8 -*-

import os
import pickle
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter

from dps.fit import interpolate
from dps.data import sort
from dps import darkphoton
from dps import sample

class DCS(object):
    """
    Class to generate fitted fixed mass dark photons.

    Any mass in the range [1 GeV, 12 GeV] can be extrapolated from the data.
    The predictions from the *css*, *nlo* and *div* methods are implemented
    first and used to define the reliable fitting domain as well as the
    transition curves separating the different regimes. Two matching procedures
    are implemented leading to an upper bound (*sup*) and a lower bound (*inf*)
    on the differential cross section.

    Parameters
    ----------
    M: float
        Mass of the dark photons (in [GeV]).
    Nz: int, optional
        Number of points along *z* for the interpolation. Default is 300.
    NkT: int, optional
        Number of points along *kT* for the interpolation. Default is 300.
    """

    def __init__(self, M, Nz=300, NkT=300, dirname="dcs/central/"):
        self.dataset = self.load_dataset(dirname)
        self.M = M
        self.check_mass()
        self.Nz = Nz
        self.NkT = NkT
        self.dp = self.init_darkphoton()
        self.fits = self.fit()
        self.kT1, self.kT2, self.kTmax = self.set_kTlimits()

    def init_darkphoton(self):
        """Instantiate a dark photon of the class mass."""

        dp = darkphoton.DarkPhoton(self.M)

        return dp

    def load_dataset(self, dirname):
        """Load the differential cross section data for all masses."""

        sort.is_clean(dirname)
        dataset = sort.sort(dirname)

        return dataset

    def check_mass(self):
        "Ensure that the darkphoton mass belongs to the data mass range."

        Mmin = self.dataset[0][0].M
        Mmax = self.dataset[0][-1].M

        text = "M out of the mass range: [{} GeV, {} Gev]".format(Mmin, Mmax)

        assert Mmin <= self.M <= Mmax, text

        return True

    def fit(self):
        """
        Set the *css*, *nlo* and *div* predictions for the class mass.

        The data at masses encapsulating the class mass are first fitted on the
        (z, kT)-plane before being interpolated to the class mass.
        """

        sample_size = (self.Nz, self.NkT)
        return interpolate.interpolate(self.dp, sample_size, self.dataset)

    def set_kTlimits(self):
        """
        Set the regime transition curves (*kT1(z)* and *kT2(z)*) and the
        reliable domain limit curve (*kTmax(z)*).
        """

        # z and kT resolution
        dz = self.dp.zmax - self.dp.zmin
        dkT = self.dp.abskTmax
        Nz = int(dz / 1e-3)
        NkT = int(dkT / 1e-3)

        # phase space grid
        z = np.linspace(self.dp.zmin, self.dp.zmax, Nz)
        kT = np.linspace(0, self.dp.abskTmax, NkT)
        kTgrid, zgrid = np.meshgrid(kT, z)
        zgrid = zgrid.flatten()
        kTgrid = kTgrid.flatten()

        # cross sections
        css = self.css(zgrid, kTgrid).reshape(Nz, NkT)
        nlo = self.nlo(zgrid, kTgrid).reshape(Nz, NkT)

        # real kTmax
        realkTmax = self.dp.kTmax(z)

        # get kT limits
        kTmax = np.zeros(Nz)
        kT1 = np.zeros(Nz)
        kT2 = np.zeros(Nz)

        for i in range(Nz):

            kTmax_, kT1_, kT2_ = self.find_kTlimits(css[i], nlo[i], kT, realkTmax[i])

            # criterium for zero cross section
            if kTmax_ - kT2_ < 0.1:
                kTmax_, kT1_, kT2_ = 0., 0., 0.

            kTmax[i], kT1[i], kT2[i] = kTmax_, kT1_, kT2_

        return self.limit_functions(z, kTmax, kT1, kT2)

    def find_kTlimits(self, css, nlo, kT, realkTmax):
        """
        Find the kT limits (kT1, kT2 and kTmax) for a fixed z.
        """

        n = len(kT)

        seek = {"kTmax": False, "kT1": True, "kT2": False}

        # init kT limits
        kTmax = 0.
        kT1 = 0.
        kT2 = 0.

        # iterate along kT
        for i in range(n-1):

            if seek["kTmax"] and nlo[i+1] > nlo[i]:
                kTmax = kT[i]
                seek["kTmax"] = False

            if seek["kT1"] and nlo[i+1] < css[i+1]:
                kT1 = kT[i]
                seek["kT1"] = False
                seek["kT2"] = True

            if seek["kT2"] and nlo[i+1] >  css[i+1]:
                kT2 = kT[i]
                seek["kT2"] = False
                seek["kTmax"] = True

        if seek["kTmax"]:
            kTmax = realkTmax

        if seek["kT2"]:
            if seek["kT1"]:
                kT1 = kTmax
                kT2 = self.dp.abskTmax + 1
            else:
                kT2 = kTmax

        # safety reduction
        kTmax = 0.95 * kTmax

        return kTmax, kT1, kT2

    def limit_functions(self, z, kTmax, kT1, kT2):
        """Smoothen and interpolate the kT limit curves."""

        b = (kTmax != 0)

        kT1 = savgol_filter(kT1[b], 201, 3)
        kT2 = savgol_filter(kT2[b], 201, 3)
        kTmax = savgol_filter(kTmax[b], 201, 3)

        fkT1 = interp1d(z[b], kT1, kind=2, bounds_error=False, fill_value=0)
        fkT2 = interp1d(z[b], kT2, kind=2, bounds_error=False, fill_value=0)
        fkTmax = interp1d(z[b], kTmax, kind=2, bounds_error=False, fill_value=0)

        return fkT1, fkT2, fkTmax

    def in_domain(self, z, kT):
        """
        Boolean function checking that (*z*, *kT*) belongs to the reliable
        region.
        """

        bin = kT < self.kTmax(z)
        bout = np.logical_not(bin)

        return bin, bout

    def css(self, z, kT):
        """
        Compute the *css* differential cross section for dark photons of
        coordinates *z* and *kT*.

        Parameters
        ----------
        z: (..., N, M) array_like
            Longitudinal fractions of the dark photons.
        kT: (..., N, M) array_like
            Transverse momentum of the dark photons (in [GeV]).

        Returns
        -------
        (..., N, M) ndarray
            *css* prediction of the differential cross section.
        """

        assert len(z) == len(kT), "Arrays of different sizes."

        return self.fits["css"](z, kT)

    def nlo(self, z, kT):
        """
        Compute the *nlo* differential cross section for dark photons of
        coordinates *z* and *kT*.

        Parameters
        ----------
        z: (..., N, M) array_like
            Longitudinal fractions of the dark photons.
        kT: (..., N, M) array_like
            Transverse momentum of the dark photons (in [GeV]).

        Returns
        -------
        (..., N, M) ndarray
            *nlo* prediction of the differential cross section.
        """

        assert len(z) == len(kT), "Arrays of different sizes."

        return self.fits["nlo"](z, kT)

    def div(self, z, kT):
        """
        Compute the *div* differential cross section for dark photons of
        coordinates *z* and *kT*.

        Parameters
        ----------
        z: (..., N, M) array_like
            Longitudinal fractions of the dark photons.
        kT: (..., N, M) array_like
            Transverse momentum of the dark photons (in [GeV]).

        Returns
        -------
        (..., N, M) ndarray
            *div* prediction of the differential cross section.
        """

        assert len(z) == len(kT), "Arrays of different sizes."

        return self.fits["div"](z, kT)

    def inf(self, z, kT):
        """
        Compute the *inf* differential cross section for dark photons of
        coordinates *z* and *kT*.

        The *inf* differential cross section is defined as *inf=css* if *kT<kT2*
        and *inf=nlo* if *kT>kT2*.

        Parameters
        ----------
        z: (..., N, M) array_like
            Longitudinal fractions of the dark photons.
        kT: (..., N, M) array_like
            Transverse momentum of the dark photons (in [GeV]).

        Returns
        -------
        (..., N, M) ndarray
            *inf* prediction of the differential cross section.
        """

        assert len(z) == len(kT), "Arrays of different sizes."

        n = len(z)
        inf = np.zeros(n)

        bin, bout = self.in_domain(z, kT)

        b1 = (kT <= self.kT1(z))
        b2 = np.logical_not(b1)

        bcss = np.logical_and(b1, bin)
        bnlo = np.logical_and(b2, bin)

        inf[bout] = 0.
        inf[bcss] = self.css(z[bcss], kT[bcss])
        inf[bnlo] = self.nlo(z[bnlo], kT[bnlo])

        return inf

    def sup(self, z, kT):
        """
        Compute the *sup* differential cross section for dark photons of
        coordinates *z* and *kT*.

        The *sup* differential cross section is defined as *sup=css* if *kT<kT2*
        and *sup=nlo* if *kT>kT2*.

        Parameters
        ----------
        z: (..., N, M) array_like
            Longitudinal fractions of the dark photons.
        kT: (..., N, M) array_like
            Transverse momentum of the dark photons (in [GeV]).

        Returns
        -------
        (..., N, M) ndarray
            *sup* prediction of the differential cross section.
        """

        assert len(z) == len(kT), "Arrays of different sizes."

        n = len(z)
        sup = np.zeros(n)

        bin, bout = self.in_domain(z, kT)

        kT1 = self.kT1(z)
        kT2 = self.kT2(z)

        b1 = (kT <= kT1)
        b2 = (kT2 < kT)
        b3 = np.logical_and(np.logical_not(b1), np.logical_not(b2))

        bcss = np.logical_and(b1, bin)
        bnlo = np.logical_and(b2, bin)
        btra = np.logical_and(b3, bin)

        sup[bout] = 0.
        sup[bcss] = self.css(z[bcss], kT[bcss])
        # sup[btra] = self.css(z[btra], kT[btra]) \
        #           + self.nlo(z[btra], kT[btra]) \
        #           - self.div(z[btra], kT[btra])
        sup[btra] = self.css(z[btra], kT[btra])
        sup[bnlo] = self.nlo(z[bnlo], kT[bnlo])

        return sup

    def max_dcs(self):

        z, kT = sample.sample(self.dp, "regular", (500, 5000),
                              mesh="constant", set_coord=False, quiet=False)
        sup = self.sup(z, kT)
        inf = self.inf(z, kT)

        isup = np.argmax(sup)
        iinf = np.argmax(inf)

        return (z[isup], kT[isup], sup[isup]), (z[iinf], kT[iinf], inf[iinf])

    def integrate(self, Nz=500, NkT=5000):
        """
        Integrate the *inf* and *sup* differential cross section over the
        whole reliable region.

        Parameters
        ----------
        Nz: int, optional
            Number of points along *z*. Default is 500.
        NkT: int, optional
            Number of points along *kT*. Default is 5000.

        Returns
        -------
        cs_inf: float
            Inferior integrated cross section.
        cs_sup: float
            Superior integrated cross section.
        """

        z, kT = sample.sample(self.dp, "regular", (Nz, NkT),
                              mesh="constant", set_coord=False, quiet=False)

        dz = (self.dp.zmax - self.dp.zmin) / Nz
        dkT = self.dp.abskTmax / NkT
        dS = dz * dkT

        cs_inf = np.sum(self.inf(z, kT) * kT) * 2 * dS
        cs_sup = np.sum(self.sup(z, kT) * kT) * 2 * dS

        return cs_inf, cs_sup

    def information_loss(self, Nz=1000):
        """
        Compute an upper bound of the ratio between the integration of the
        differential cross section outside and inside of the reliable region
        for the *inf* and the *sup* schemes.

        Parameters
        ----------
        Nz: int, optional
            Number of points along *z*. Default is 1000.

        Returns
        -------
        r_inf: float
            Inferior ratio.
        r_sup: float
            Superior ratio.
        """
        # total cross section
        cs_inf, cs_sup = self.integrate()

        # upper bound on the neglected cross section
        z = np.linspace(self.dp.zmin, self.dp.zmax, Nz)
        dz = z[1] - z[0]
        realkTmax = self.dp.kTmax(z)
        kTmax = 0.9 * self.kTmax(z)

        loss_inf = np.sum((realkTmax - kTmax) * self.inf(z, kTmax) * 2 * kTmax) * dz
        loss_sup = np.sum((realkTmax - kTmax) * self.sup(z, kTmax) * 2 * kTmax) * dz

        return loss_inf / cs_inf, loss_sup / cs_sup

\begin{thebibliography}{10}

\bibitem{SHiP}
Sergey Alekhin et~al.
\newblock {A facility to Search for Hidden Particles at the CERN SPS: the SHiP
  physics case}.
\newblock {\em Rept. Prog. Phys.}, 79(12):124201, 2016.

\bibitem{Collins1985}
John~C. Collins, Davison~E. Soper, and George~F. Sterman.
\newblock {Transverse Momentum Distribution in Drell-Yan Pair and W and Z Boson
  Production}.
\newblock {\em Nucl. Phys.}, B250:199--224, 1985.

\bibitem{US}
Marco Battaglieri et~al.
\newblock {US Cosmic Visions: New Ideas in Dark Matter 2017: Community Report}.
\newblock 2017.

\bibitem{SeaQuest}
Asher Berlin, Stefania Gori, Philip Schuster, and Natalia Toro.
\newblock {Dark Sectors at the Fermilab SeaQuest Experiment}.
\newblock 2018.

\bibitem{Pospelov}
Brian Batell, Maxim Pospelov, and Adam Ritz.
\newblock Exploring portals to a hidden sector through fixed targets.
\newblock {\em Phys. Rev. D}, 80:095024, Nov 2009.

\bibitem{Blumlein}
Johannes Blümlein and Jürgen Brunner.
\newblock {New Exclusion Limits on Dark Gauge Forces from Proton Bremsstrahlung
  in Beam-Dump Data}.
\newblock {\em Phys. Lett.}, B731:320--326, 2014.

\bibitem{FieldBook}
R.~D. Field.
\newblock {\em Applications of perturbative QCD}.
\newblock Addison-Wesley, 1989.

\bibitem{Handbook}
George Sterman, John Smith, John~C. Collins, James Whitmore, Raymond Brock,
  Joey Huston, Jon Pumplin, Wu-Ki Tung, Hendrik Weerts, Chien-Peng Yuan,
  Stephen Kuhlmann, Sanjib Mishra, Jorge~G. Morf\'{\i}n, Fredrick Olness,
  Joseph Owens, Jianwei Qiu, and Davison~E. Soper.
\newblock Handbook of perturbative qcd.
\newblock {\em Rev. Mod. Phys.}, 67:157--248, Jan 1995.

\bibitem{Collins2003}
John~C. Collins.
\newblock {What exactly is a parton density ?}
\newblock 2003.

\bibitem{Collins1989}
John~C. Collins, Davison~E. Soper, and George~F. Sterman.
\newblock {Factorization of Hard Processes in QCD}.
\newblock {\em Adv. Ser. Direct. High Energy Phys.}, 5:1--91, 1989.

\bibitem{Maltoni}
Fabio Maltoni, Thomas McElmurry, Robert Putman, and Scott Willenbrock.
\newblock {Choosing the Factorization Scale in Perturbative QCD}.
\newblock 2007.

\bibitem{EllisBook}
R.~K. Ellis, W.~J. Stirling, and B.~R. Webber.
\newblock {\em QCD and collider physics}.
\newblock Cambridge University Press, Trumpington Street, 1996.

\bibitem{PerfectDY}
Ricardo~A. Rodriguez-Pedraza.
\newblock {QCD resummation for the fully differential Drell- Yan cross
  section}.
\newblock {\em Retrospective Theses and Dissertations}, 15944, 2007.

\bibitem{Landry2003}
F.~Landry, R.~Brock, Pavel~M. Nadolsky, and C.~P. Yuan.
\newblock {Tevatron Run-1 $Z$ boson data and Collins-Soper-Sterman resummation
  formalism}.
\newblock {\em Phys. Rev.}, D67:073016, 2003.

\bibitem{Konychev2006}
Anton~V. Konychev and Pavel~M. Nadolsky.
\newblock {Universality of the Collins-Soper-Sterman nonperturbative function
  in gauge boson production}.
\newblock {\em Phys. Lett.}, B633:710--714, 2006.

\bibitem{Boglione2014}
M.~Boglione, J.~O. Gonzalez~Hernandez, S.~Melis, and A.~Prokudin.
\newblock {A study on the interplay between perturbative QCD and CSS/TMD
  formalism in SIDIS processes}.
\newblock {\em JHEP}, 02:095, 2015.

\bibitem{LHAPDF6}
Andy Buckley, James Ferrando, Stephen Lloyd, Karl Nordström, Ben Page, Martin
  Rüfenacht, Marek Schönherr, and Graeme Watt.
\newblock {LHAPDF6: parton density access in the LHC precision era}.
\newblock {\em Eur. Phys. J.}, C75:132, 2015.

\bibitem{CT14}
Sayipjamal Dulat, Tie-Jiun Hou, Jun Gao, Marco Guzzi, Joey Huston, Pavel
  Nadolsky, Jon Pumplin, Carl Schmidt, Daniel Stump, and C.~P. Yuan.
\newblock {New parton distribution functions from a global analysis of quantum
  chromodynamics}.
\newblock {\em Phys. Rev.}, D93(3):033006, 2016.

\bibitem{Hamberg_1990}
R.~Hamberg, W.~L. van Neerven, and T.~Matsuura.
\newblock {A complete calculation of the order $\alpha-s^{2}$ correction to the
  Drell-Yan $K$ factor}.
\newblock {\em Nucl. Phys.}, B359:343--405, 1991.
\newblock [Erratum: Nucl. Phys.B644,403(2002)].

\end{thebibliography}
